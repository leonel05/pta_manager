@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Liste des tâches</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Liste des tâches')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="tools" >
                        <a href="{{ route('taches.create') }}" class="btn btn-success" style=""><i class="glyphicon glyphicon-plus"></i>
                            Ajouter
                        </a>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Identifiant</th>
                                <th>Tâches</th>
                                <th>Durée (Mois)</th>
                                <th>Niveau</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($taches as $tache)
                                <tr>
                                    <td>{{ $tache->code }}</td>
                                    <td>{{ $tache->libelle }}</td>
                                    <td>{{ $tache->nb_mois }}</td>
                                    <td>{{ $tache->niveau }}</td>
                                    <td>
                                        {{--<a href="{{ route('taches.edit', ['id' => $tache->id]) }}" class="btn-sm btn-primary"
                                           title="Supprimer"><i class="fa fa-edit"></i></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection