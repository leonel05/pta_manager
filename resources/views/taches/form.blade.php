<?php
$readonly = "";
$disabled = "";
if ($tache->id) {  // Cas de la modification ou de la consultation
    if (isset($isView) && $isView) { // Cas de la consultation
        $options = ['class' => 'form-horizontal'];
        $readonly = "readonly";
    } else { // Cas de la modification
        $options = ['route' => ['taches.update', $tache->id], 'method' => 'PUT', 'class' => 'form-horizontal'];
    }
} else { // Cas de l'ajout
    $options = ['route' => ['taches.store'], 'method' => 'POST', 'class' => 'form-horizontal'];
    $disabled = "disabled";
}
?>

@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Nouvelle Tâche</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Nouvelle Tâche')])
@endsection

@section('page-content')
    <div class="portlet light">
        {!! Form::model($tache, $options) !!}
        <div class="form-actions top margin-bottom-20">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="btn-group">
                        <a href="{{ route('taches.index') }}" class="btn btn-default ">
                            <i class="fa fa-arrow-left"></i> Retour
                        </a>
                        @if(isset($isView) && $isView)
                            <a href="{{ route('taches.edit', ['id' => $tache->id]) }}" class="btn btn-primary">
                                <i class="fa fa-edit"></i> Modifier
                            </a>
                        @endif

                        {{--Cacher l'affichage du bouton enregistrer lors de la consultation--}}
                        @unless(isset($isView) && $isView)
                            <button type="submit" class="btn green"><i class="fa fa-save"></i> Enregistrer</button>
                        @endunless
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body">
            @if(isset($message))
                <div class="col-sm-offset-2 col-sm-8 alert alert-{{ $type }}">
                    {{ $message }}
                </div>
            @endif
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('code', 'Code de la tâche', ['class' => 'text-primary label']) !!}
                    {!! Form::text('code', null, ['class' => 'form-control', 'disabled']) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('code_act', 'Code de l\'activité', ['class' => 'text-primary label']) !!} <span
                            class="text-danger">*</span>
                    {!! Form::select('code_act', $act_codes, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', 'required' => 'required', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('debut', 'Période d\'exécution', ['class' => 'text-primary label']) !!} <span
                            class="text-danger">*</span>
                </div>
                <div class="col-sm-offset-2 col-sm-4">
                    {!! Form::select('debut', $mois, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', 'required' => 'required', $readonly]) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::select('fin', $mois, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', 'required' => 'required', $readonly]) !!}
                </div>

            </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-4">
                        {!! Form::label('montant', 'Montant alloué', ['class' => 'text-primary label']) !!}
                        <span class="text-danger">*</span>
                        {!! Form::number('montant', null, ['class' => 'form-control', 'min' => '0', 'required' => 'required', $readonly]) !!}
                    </div>
                    <div class="col-sm-4">
                        {!! Form::label('poids', 'Poids Global', ['class' => 'text-primary label']) !!}
                        <span class="text-danger">*</span>
                        {!! Form::number('poids', null, ['class' => 'form-control', 'min' => '0', 'max' => '100', 'required' => 'required', $readonly]) !!}
                    </div>
                </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('libelle', 'Libellé de la tâche', ['class' => 'text-primary label']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::textarea('libelle', null, ['class' => 'form-control', 'rows' => '3', 'required' => 'required', $readonly]) !!}
                </div>
            </div>

            @unless(isset($isView) && $isView)
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <span class="text-danger required-asterik">*</span> Champs obligatoires
                    </div>
                </div>
            @endunless
        </div>
        {!! Form::close() !!}
    </div>
@endsection