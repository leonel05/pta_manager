@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Liste des rapports d'evaluation</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Liste des rapports d\'evaluation')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="form-actions top margin-bottom-20">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="btn-group">
                                    <a href="{{ route('rapports.index') }}" class="btn btn-default ">
                                        <i class="fa fa-arrow-left"></i> Retour
                                    </a>
                                    <a href="{{ route('rapports.edit', ['id' => $rapport->id]) }}" class="btn btn-primary">
                                        <i class="fa fa-edit"></i> Modifier
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="col-xs-12 bg-grey" style="padding: 20px;">
                        <h3 class="text-center"><u><b>RAPPORT D’AUTO-EVALUATION</b></u></h3>
                        <br />
                        @if(!empty($ministere))
                            <h4 class="text-center"><b>{{ $ministere->libelle }} ({{ $ministere->code }})</b></h4>
                        @else
                            <div class="col-sm-12 text-center">
                                <b>MINISTERE :</b> <i>{{ $details_pta[2] }}</i>
                            </div>
                        @endif
                        <br /><br />
                        <div class="col-sm-12 text-center">
                            <b>STRUCTURE :</b> <i>{{ $structure }}</i>
                        </div>
                        <br /><br />
                        <div class="col-sm-6 text-center">
                            <b>ANNEE :</b> <i>{{ $details_pta[1] }}</i>
                        </div>
                        <div class="col-sm-6 text-center">
                            <b>TYPE DE RAPPORT :</b> <i>{{ $evaluation->type }}</i>
                        </div>
                        <br /><br />
                        <div class="col-sm-6 text-center">
                            <b>NOM DU RESPONSABLE :</b> <i>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  </i>
                        </div>
                        <div class="col-sm-6 text-center">
                            <b>NOM DE L’ELABORATEUR DU DOCUMENT  :</b> <i>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </i>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="text-primary" style="font-weight: bold;">RESUME</h4>
                            <p>{{ $rapport->resume }}</p>
                        </div>
                        <div class="col-xs-12">
                            <h4 class="text-primary" style="font-weight: bold;">INTRODUCTION</h4>
                            <p>{{ $rapport->introduction }}</p>
                        </div>
                        <div class="col-xs-12">
                            <h4 class="text-primary" style="font-weight: bold;">I. &nbsp; &nbsp; OBJECTIFS ET RESULTATS ATTENDUS</h4>
                            <h4 class="text-primary">1. &nbsp; &nbsp; Les objectifs et les résultats attendus</h4>
                            <h4 class="text-primary">1.1 &nbsp; &nbsp; Les objectifs</h4>
                            <p>{{ $rapport->objectifs }}</p>
                        </div>
                        <div class="col-xs-12">
                            <h4 class="text-primary">1.2 &nbsp; &nbsp; Les résultats attendus</h4>
                            <p>{{ $rapport->resultats }}</p>
                        </div>
                        <div class="col-xs-12">
                            <h4 class="text-primary">2 &nbsp; &nbsp; Les ressources allouées</h4>
                            <p>{{ $rapport->resources }}</p>
                        </div>
                        <div class="col-xs-12">
                            <h4 class="text-primary" style="font-weight: bold;">II. &nbsp; &nbsp; LE BILAN PHYSIQUE D’ACTIVITES</h4>
                            <h4 class="text-primary">1. &nbsp; &nbsp; Point d’exécution des activités prévues au cours de l’année</h4>
                            <p>Cf. Tableau d'évaluation correspondant.</p>
                        </div>
                        <div class="col-xs-12">
                            <h4 class="text-primary">2 &nbsp; &nbsp; Commentaire du taux d’exécution physique du PTA</h4>
                            <p>{{ $rapport->activites_prevues }}</p>
                        </div>
                        <div class="col-xs-12">
                            <h4 class="text-primary">3 &nbsp; &nbsp; Activités non prévues mais réalisées sur ressources du Budget-Programme</h4>
                            <p>{{ $rapport->activites_non_prevues }}</p>
                        </div>
                        <div class="col-xs-12">
                            <h4 class="text-primary" style="font-weight: bold;">III. &nbsp; &nbsp; BILAN FINANCIER</h4>
                            <p>{{ $rapport->bilan_financier }}</p>
                        </div>
                        <div class="col-xs-12">
                            <h4 class="text-primary" style="font-weight: bold;">IV. &nbsp; &nbsp; DIFFICULTES RENCONTREES ET RECOMMANDATIONS</h4>
                            <p>{{ $rapport->difficultes_recommandations }}</p>
                        </div>
                        <div class="col-xs-12">
                            <h4 class="text-primary" style="font-weight: bold;">CONCLUSION</h4>
                            <p>{{ $rapport->conclusion }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection