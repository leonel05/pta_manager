<?php
$readonly = "";
$disabled = "";
if ($rapport->id) {  // Cas de la modification ou de la consultation
    if (isset($isView) && $isView) { // Cas de la consultation
        $options = ['class' => 'form-horizontal'];
        $readonly = "readonly";
    } else { // Cas de la modification
        $options = ['route' => ['rapports.update', $rapport->id], 'method' => 'PUT', 'class' => 'form-horizontal'];
        $disabled = "disabled";
    }
} else { // Cas de l'ajout
    $options = ['route' => ['rapports.store'], 'method' => 'POST', 'class' => 'form-horizontal'];
    $disabled = "disabled";
}
?>

@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Nouveau rapport</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'Rapports', 'Nouveau rapport')])
@endsection

@section('page-content')
    <div class="portlet light">
        {!! Form::model($rapport, $options) !!}
        <div class="form-actions top margin-bottom-20">
            <div class="row">
                <div class="col-md-8">
                    <div class="btn-group">
                        <a href="{{ route('rapports.index') }}" class="btn btn-default ">
                            <i class="fa fa-arrow-left"></i> Retour
                        </a>
                        @if(isset($isView) && $isView)
                            <a href="{{ route('rapports.edit', ['id' => $rapport->id]) }}" class="btn btn-primary">
                                <i class="fa fa-edit"></i> Modifier
                            </a>
                        @endif

                        {{--Cacher l'affichage du bouton enregistrer lors de la consultation--}}
                        @unless(isset($isView) && $isView)
                            <button type="submit" class="btn green"><i class="fa fa-save"></i> Enregistrer</button>
                        @endunless
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body">
            @if(isset($message))
                <div class="col-sm-offset-2 col-sm-8 alert alert-{{ $type }}">
                    {{ $message }}
                </div>
            @endif
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('evaluation', 'TABLEAU D\'EVALUATION', ['class' => 'text-primary label', 'style' => 'font-weight: bold;']) !!} <span
                            class="text-danger">*</span>
                        {!! Form::select('evaluation', $evaluations, $evaluation, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', 'required' => 'required', $disabled]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    {!! Form::label('resume', 'RESUME', ['class' => 'text-primary', 'style' => 'font-weight: bold;']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::textarea('resume', null, ['class' => 'form-control', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    {!! Form::label('introduction', 'INTRODUCTION', ['class' => 'text-primary', 'style' => 'font-weight: bold;']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::textarea('introduction', null, ['class' => 'form-control', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    {!! Form::label('objectifs', 'OBJECTIFS', ['class' => 'text-primary', 'style' => 'font-weight: bold;']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::textarea('objectifs', null, ['class' => 'form-control', $readonly]) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::label('resultats', 'RESULTATS', ['class' => 'text-primary', 'style' => 'font-weight: bold;']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::textarea('resultats', null, ['class' => 'form-control', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    {!! Form::label('resources', 'RESOURCES ALLOUEES', ['class' => 'text-primary', 'style' => 'font-weight: bold;']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::textarea('resources', null, ['class' => 'form-control', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    {!! Form::label('activites_prevues', 'COMMENTAIRES', ['class' => 'text-primary', 'style' => 'font-weight: bold;']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::textarea('activites_prevues', null, ['class' => 'form-control', $readonly]) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::label('activites_non_prevues', 'ACTIVITES NON PREVUES', ['class' => 'text-primary', 'style' => 'font-weight: bold;']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::textarea('activites_non_prevues', null, ['class' => 'form-control', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-6">
                    {!! Form::label('bilan_financier', 'BILAN FINANCIER', ['class' => 'text-primary', 'style' => 'font-weight: bold;']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::textarea('bilan_financier', null, ['class' => 'form-control', $readonly]) !!}
                </div>
                <div class="col-sm-6">
                    {!! Form::label('difficultes_recommandations', 'DIFFICULTES RENCONTREES ET RECOMMANDATIONS', ['class' => 'text-primary', 'style' => 'font-weight: bold;']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::textarea('difficultes_recommandations', null, ['class' => 'form-control', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-12">
                    {!! Form::label('conclusion', 'CONCLUSION', ['class' => 'text-primary', 'style' => 'font-weight: bold;']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::textarea('conclusion', null, ['class' => 'form-control', $readonly]) !!}
                </div>
            </div>

            @unless(isset($isView) && $isView)
                <div class="row">
                    <div class="col-md-8">
                        <span class="text-danger required-asterik">*</span> Champs obligatoires
                    </div>
                </div>
            @endunless
        </div>
        {!! Form::close() !!}
    </div>
@endsection