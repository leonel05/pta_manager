@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Liste des rapports d'evaluation</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Liste des rapports d\'evaluation')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="tools" >
                        <a href="{{ route('rapports.create') }}" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i>
                            Ajouter
                        </a>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Code du PTA</th>
                                <th>Titre</th>
                                <th>N° Période</th>
                                <th>Type</th>
                                <th>Date de création</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rapports as $rapport)
                                <tr>
                                    <td>{{ $evaluations[$rapport->id]->code_pta }}</td>
                                    <td>Rapport du {{ $evaluations[$rapport->id]->titre }}</td>
                                    <td>{{ $evaluations[$rapport->id]->periode }}</td>
                                    <td><span class="label label-default">{{ $evaluations[$rapport->id]->type }}</span></td>
                                    <td>{{ $rapport->created_at }}</td>
                                    <td>
                                        <a href="{{ route('rapports.show', ['id' => $rapport->id]) }}" class="btn-sm btn-success"
                                           title="Consulter"><i class="fa fa-eye"></i></a>
                                        <a href="{{ route('rapports.edit', ['id' => $rapport->id]) }}" class="btn-sm btn-primary"
                                           title="Modifier"><i class="fa fa-edit"></i></a>
                                        {{--<a href="{{ route('rapports.destroy', ['id' => $rapport->id]) }}" class="btn-sm btn-danger"
                                           title="Supprimer"><i class="fa fa-trash-o"></i></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection