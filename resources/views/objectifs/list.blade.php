@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Liste des objectifs & résultats</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Liste des objectifs & résultats')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="tools" >
                        <a href="{{ route('objectifs.create') }}" class="btn btn-success" style=""><i class="glyphicon glyphicon-plus"></i>
                            Ajouter
                        </a>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Identifiant</th>
                                <th>Objectif</th>
                                <th>Résultat</th>
                                <th>Période</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($objectifs as $objectif)
                                <tr>
                                    <td>{{ $objectif->code }}</td>
                                    <td>{{ $objectif->libelle }}</td>
                                    <td>{{ $objectif->resultat }}</td>
                                    <td>{{ $objectif->nb_mois }}</td>
                                    <td>
                                        {{--<a href="{{ route('objectifs.destroy', ['id' => $objectif->id]) }}" class="btn-sm btn-danger"
                                           title="Supprimer"><i class="fa fa-trash-o"></i></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection