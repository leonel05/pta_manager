@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Liste des activités</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Liste des activités')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="tools" >
                        <a href="{{ route('activites.create') }}" class="btn btn-success" style=""><i class="glyphicon glyphicon-plus"></i>
                            Ajouter
                        </a>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>Identifiant</th>
                                <th>Activites</th>
                                <th>Durée (Mois)</th>
                                <th>Niveau</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($activites as $activite)
                                <tr>
                                    <td>{{ $activite->code }}</td>
                                    <td>{{ $activite->libelle }}</td>
                                    <td>{{ $activite->nb_mois }}</td>
                                    <td>{{ $activite->niveau }}</td>
                                    <td>
                                        {{--<a href="{{ route('activites.edit', ['id' => $activite->id]) }}" class="btn-sm btn-primary"
                                           title="Supprimer"><i class="fa fa-edit"></i></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection