<?php
$readonly = "";
$disabled = "";
if ($evaluation->id) {  // Cas de la modification ou de la consultation
    if (isset($isView) && $isView) { // Cas de la consultation
        $options = ['class' => 'form-horizontal'];
        $readonly = "readonly";
    } else { // Cas de la modification
        $options = ['route' => ['evaluations.update', $evaluation->id], 'method' => 'PUT', 'class' => 'form-horizontal'];
    }
}
?>


@extends('layouts.app-content')

@section('page-title')
    Programme de Travail Annuel
    <small>Tableau d'Evaluation</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Tableau d\'Evaluation')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    {!! Form::model($evaluation, $options) !!}
                    <div class="row">
                        <div class="col-sm-4 btn-group">
                            <a href="{{ route('evaluations.index') }}" class="btn btn-default ">
                                <i class="fa fa-arrow-left"></i> Retour
                            </a>
                            @if(isset($isView) && $isView)
                                <a href="{{ route('evaluations.edit', ['id' => $evaluation->id]) }}" class="btn btn-primary">
                                    <i class="fa fa-edit"></i> Modifier
                                </a>
                            @endif

                            {{--Cacher l'affichage du bouton enregistrer lors de la consultation--}}
                            @unless(isset($isView) && $isView)
                                <button type="submit" class="btn green"><i class="fa fa-save"></i> Enregistrer les modifications</button>
                            @endunless
                        </div>
                        <div class="col-sm-4 text-center">
                            <label><b>MINISTERE : &nbsp;</b></label> {{ $ministere }}
                        </div>
                        <div class="col-sm-4 text-center">
                            <label><b>EXERCICE : &nbsp;</b></label> {{ $annee }}
                        </div>
                        <div class="col-sm-8 text-center">
                            <label><b>STRUCTURE : &nbsp;</b></label> -
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-12 text-center text-primary">
                            <h4>{{ $evaluation->titre }}</h4>
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered text-center">
                            <thead class="text-center bg-blue-dark">
                            <tr>
                                <td rowspan="2">CODE</td>
                                <td rowspan="2">Objectifs(O) Actions(A) Activités(a) Taches(t)</td>
                                <td rowspan="2">Poids Global (%)</td>
                                <td rowspan="2">Opérations physiques/Activités réalisées)</td>
                                <td rowspan="2"  width="80px">Niveau de réalisation physique (%)</td>
                                <td colspan="5">Montant (F CFA)</td>
                                <td rowspan="2">Observations (Retard, mesures préconisées)</td>
                            </tr>
                            <tr>
                                <td>Alloué</td>
                                <td width="100px">Engagé</td>
                                <td>% Engagé</td>
                                <td width="100px">Ordonnancé</td>
                                <td>% Ordonnancé</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($programmes as $programme)
                                <tr class="bg-grey-steel">
                                    <td>PROG</td>
                                    <td colspan="10">{{ $programme->libelle.' : '.$programme->objectif }}</td>
                                </tr>
                                @foreach($objectifs[$programme->code] as $objectif)
                                    <tr style="font-weight: bold;">
                                        <td>{{ $objectif->abbrev }}</td>
                                        <td>{{ $objectif->libelle }}</td>
                                        <td>{{ $objectif->poids }}</td>
                                        <td><textarea name="objectifs[{{ $objectif->id }}][operation]" class="form-control" style="resize: none;" {{ $readonly }}>{{ $objectif->operations }}</textarea></td>
                                        <td><input type="number" name="objectifs[{{ $objectif->id }}][niveau]" value="{{ $objectif->niveau }}" class="form-control" min="0" {{ $readonly }}></td>
                                        <td>{{ $objectif->montant }}</td>
                                        <td><input type="number" name="objectifs[{{ $objectif->id }}][engage]" value="{{ $objectif->mnt_engage }}" class="form-control" min="0" {{ $readonly }}></td>
                                        <td>{{ $objectif->taux_engage }}</td>
                                        <td><input type="number" name="objectifs[{{ $objectif->id }}][ordonance]" value="{{ $objectif->mnt_ordonance }}" class="form-control" min="0" {{ $readonly }}></td>
                                        <td>{{ $objectif->taux_ordonance }}</td>
                                        <td><textarea name="objectifs[{{ $objectif->id }}][observation]" class="form-control" style="resize: none;" {{ $readonly }}>{{ $objectif->observations }}</textarea></td>
                                    </tr>
                                    <tr style="font-weight: bold; font-style: italic;">
                                        <td>R</td>
                                        <td colspan="10">{{ $objectif->resultat }}</td>
                                        <!--td>{{ $objectif->poids }}</td>
                                        <td><textarea name="" class="form-control" rows="3" style="resize: none;" disabled></textarea></td>
                                        <td><input type="number" name="" class="form-control" min="0" disabled></td>
                                        <td>{{ $objectif->montant }}</td>
                                        <td><input type="number" name="" class="form-control" min="0" disabled></td>
                                        <td><input type="number" name="" class="form-control" min="0" disabled></td>
                                        <td><textarea name="" class="form-control" rows="3" style="resize: none;" disabled></textarea></td-->
                                    </tr>
                                        @foreach($actions[$objectif->code] as $action)
                                            <tr style="font-style: italic; color: orange;">
                                                <td>{{ $action->abbrev }}</td>
                                                <td>{{ $action->libelle }}</td>
                                                <td>{{ $action->poids }}</td>
                                                <td><textarea name="actions[{{ $action->id }}][operation]" class="form-control" style="resize: none;" {{ $readonly }}>{{ $action->operations }}</textarea></td>
                                                <td><input type="number" name="actions[{{ $action->id }}][niveau]" value="{{ $action->niveau }}" class="form-control" min="0" {{ $readonly }}></td>
                                                <td>{{ $action->montant }}</td>
                                                <td><input type="number" name="actions[{{ $action->id }}][engage]" value="{{ $action->mnt_engage }}" class="form-control" min="0" {{ $readonly }}></td>
                                                <td>{{ $action->taux_engage }}</td>
                                                <td><input type="number" name="actions[{{ $action->id }}][ordonance]" value="{{ $action->mnt_ordonance }}" class="form-control" min="0" {{ $readonly }}></td>
                                                <td>{{ $action->taux_ordonance }}</td>
                                                <td><textarea name="actions[{{ $action->id }}][observation]" class="form-control" style="resize: none;" {{ $readonly }}>{{ $action->observations }}</textarea></td>
                                            </tr>
                                            @foreach($activites[$action->code] as $activite)
                                                <tr style="font-style: italic; color: green;">
                                                    <td>{{ $activite->abbrev }}</td>
                                                    <td>{{ $activite->resultat }}</td>
                                                    <td>{{ $activite->poids }}</td>
                                                    <td><textarea name="activites[{{ $activite->id }}][operation]" class="form-control" style="resize: none;" {{ $readonly }}>{{ $activite->operations }}</textarea></td>
                                                    <td><input type="number" name="activites[{{ $activite->id }}][niveau]" value="{{ $activite->niveau }}" class="form-control" min="0" {{ $readonly }}></td>
                                                    <td>{{ $activite->montant }}</td>
                                                    <td><input type="number" name="activites[{{ $activite->id }}][engage]" value="{{ $activite->mnt_engage }}" class="form-control" min="0" {{ $readonly }}></td>
                                                    <td>{{ $activite->taux_engage }}</td>
                                                    <td><input type="number" name="activites[{{ $activite->id }}][ordonance]" value="{{ $activite->mnt_ordonance }}" class="form-control" min="0" {{ $readonly }}></td>
                                                    <td>{{ $activite->taux_ordonance }}</td>
                                                    <td><textarea name="activites[{{ $activite->id }}][observation]" class="form-control" style="resize: none;" {{ $readonly }}>{{ $activite->observations }}</textarea></td>
                                                </tr>
                                                @foreach($taches[$activite->code] as $tache)
                                                    <tr style="font-style: italic; color: #555;">
                                                        <td>{{ $tache->abbrev }}</td>
                                                        <td>{{ $tache->resultat }}</td>
                                                        <td>{{ $tache->poids }}</td>
                                                        <td><textarea name="taches[{{ $tache->id }}][operation]" class="form-control" style="resize: none;" {{ $readonly }}>{{ $tache->operations }}</textarea></td>
                                                        <td><input type="number" name="taches[{{ $tache->id }}][niveau]" value="{{ $tache->niveau }}" class="form-control" min="0" {{ $readonly }}></td>
                                                        <td>{{ $tache->montant }}</td>
                                                        <td><input type="number" name="taches[{{ $tache->id }}][engage]" value="{{ $tache->mnt_engage }}" class="form-control" min="0" {{ $readonly }}></td>
                                                        <td>{{ $tache->taux_engage }}</td>
                                                        <td><input type="number" name="taches[{{ $tache->id }}][ordonance]" value="{{ $tache->mnt_ordonance }}" class="form-control" min="0" {{ $readonly }}></td>
                                                        <td>{{ $tache->taux_ordonance }}</td>
                                                        <td><textarea name="taches[{{ $tache->id }}][observation]" class="form-control" style="resize: none;" {{ $readonly }}>{{ $tache->observations }}</textarea></td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        @endforeach
                                @endforeach
                            @endforeach
                                @unless(isset($isView) && $isView)
                                <tr>
                                    <td colspan="11">
                                        <button type="submit" class="btn green btn-block"><b>Mise à jour des informations du tableau d'évaluation</b></button>
                                    </td>
                                </tr>
                                @endunless
                            </tbody>
                        </table>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection