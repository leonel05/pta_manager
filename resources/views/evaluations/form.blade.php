<?php
$readonly = "";
$disabled = "";
if ($evaluation->id) {  // Cas de la modification ou de la consultation
    if (isset($isView) && $isView) { // Cas de la consultation
        $options = ['class' => 'form-horizontal'];
        $readonly = "readonly";
    } else { // Cas de la modification
        $options = ['route' => ['evaluations.update', $evaluation->id], 'method' => 'PUT', 'class' => 'form-horizontal'];
    }
} else { // Cas de l'ajout
    $options = ['route' => ['evaluations.store'], 'method' => 'POST', 'class' => 'form-horizontal'];
    $disabled = "disabled";
}
?>

@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Nouveau Tableau d'Evaluation</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Nouveau Tableau d\'Evaluation')])
@endsection

@section('page-content')
    <div class="portlet light">
        {!! Form::model($evaluation, $options) !!}
        <div class="form-actions top margin-bottom-20">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="btn-group">
                        <a href="{{ route('evaluations.index') }}" class="btn btn-default ">
                            <i class="fa fa-arrow-left"></i> Retour
                        </a>
                        @if(isset($isView) && $isView)
                            <a href="{{ route('evaluations.edit', ['id' => $evaluation->id]) }}" class="btn btn-primary">
                                <i class="fa fa-edit"></i> Modifier
                            </a>
                        @endif

                        {{--Cacher l'affichage du bouton enregistrer lors de la consultation--}}
                        @unless(isset($isView) && $isView)
                            <button type="submit" class="btn green"><i class="fa fa-save"></i> Enregistrer</button>
                        @endunless
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body">
            @if(isset($message))
                <div class="col-sm-offset-2 col-sm-8 alert alert-{{ $type }}">
                    {{ $message }}
                </div>
            @endif
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('code_pta', 'Code du PTA', ['class' => 'text-primary label']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::select('code_pta', $pta_codes, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', 'required' => 'required', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('titre', 'Intitulé du tableau d\'évaluation', ['class' => 'text-primary label']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::text('titre', null, ['class' => 'form-control', 'required' => 'required', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    {!! Form::label('type', 'Périodicité (Type de tableau d\'évaluation)', ['class' => 'text-primary label']) !!}
                    <span class="text-danger">*</span>
                    {!! Form::select('type', $types, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', 'required' => 'required', $readonly]) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::label('periode', 'N° de la période', ['class' => 'text-primary label']) !!}
                    {!! Form::number('periode', 0, ['class' => 'form-control', 'min' => '0', $readonly]) !!}
                </div>
            </div>

            @unless(isset($isView) && $isView)
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <span class="text-danger required-asterik">*</span> Champs obligatoires
                    </div>
                </div>
            @endunless
        </div>
        {!! Form::close() !!}
    </div>
@endsection