@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Liste des tableaux d'evaluation</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Liste des tableaux d\'evaluation')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="tools" >
                        <a href="{{ route('evaluations.create') }}" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i>
                            Ajouter
                        </a>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Code du PTA</th>
                                <th>Titre</th>
                                <th>N° Période</th>
                                <th>Type</th>
                                <th>Date de création</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($evaluations as $evaluation)
                                <tr>
                                    <td>{{ $evaluation->code_pta }}</td>
                                    <td>{{ $evaluation->titre }}</td>
                                    <td>{{ $evaluation->periode }}</td>
                                    <td><span class="label label-default">{{ $evaluation->type }}</span></td>
                                    <td>{{ $evaluation->date_creation }}</td>
                                    <td>
                                        <a href="{{ route('evaluations.show', ['id' => $evaluation->id]) }}" class="btn-sm btn-success"
                                           title="Consulter"><i class="fa fa-eye"></i></a>
                                        <a href="{{ route('evaluations.edit', ['id' => $evaluation->id]) }}" class="btn-sm btn-primary"
                                           title="Modifier"><i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection