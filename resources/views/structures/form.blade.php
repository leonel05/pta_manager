<?php
$readonly = "";
$disabled = "";
if ($structure->id) {  // Cas de la modification ou de la consultation
    if (isset($isView) && $isView) { // Cas de la consultation
        $options = ['class' => 'form-horizontal'];
        $readonly = "readonly";
    } else { // Cas de la modification
        $options = ['route' => ['structures.update', $structure->id], 'method' => 'PUT', 'class' => 'form-horizontal'];
    }
} else { // Cas de l'ajout
    $options = ['route' => ['structures.store'], 'method' => 'POST', 'class' => 'form-horizontal'];
    $disabled = "disabled";
}
?>

@extends('layouts.app-content')

@section('page-title')
    Paramètres de base
    <small>Ajout d'une nouvelle structure</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'Paramètres de base', 'Nouvelle structure')])
@endsection

@section('page-content')
    <div class="portlet light">
        {!! Form::model($structure, $options) !!}
        <div class="form-actions top margin-bottom-20">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="btn-group">
                        <a href="{{ route('structures.index') }}" class="btn btn-default ">
                            <i class="fa fa-arrow-left"></i> Retour
                        </a>
                        @if(isset($isView) && $isView)
                            <a href="{{ route('structures.edit', ['id' => $structure->id]) }}" class="btn btn-primary">
                                <i class="fa fa-edit"></i> Modifier
                            </a>
                        @endif

                        {{--Cacher l'affichage du bouton enregistrer lors de la consultation--}}
                        @unless(isset($isView) && $isView)
                            <button type="submit" class="btn green"><i class="fa fa-save"></i> Enregistrer</button>
                        @endunless
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body">
            @if(isset($message))
                <div class="col-sm-offset-2 col-sm-8 alert alert-{{ $type }}">
                    {{ $message }}
                </div>
            @endif
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('code', 'Code de la structure', ['class' => 'text-primary label']) !!}
                    {!! Form::text('code', null, ['class' => 'form-control', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('libelle', 'Libellé de la structure', ['class' => 'text-primary label']) !!}
                    {!! Form::text('libelle', null, ['class' => 'form-control', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('code_typestructure', 'Code (Type de la structure)', ['class' => 'text-primary label']) !!} <span
                            class="text-danger">*</span>
                    {!! Form::select('code_typestructure', $typestructures, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', 'required' => 'required', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('code_ministere', 'Code du minitère', ['class' => 'text-primary label']) !!} <span
                            class="text-danger">*</span>
                    {!! Form::select('code_ministere', $ministeres, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', 'required' => 'required', $readonly]) !!}
                </div>
            </div>

            @unless(isset($isView) && $isView)
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <span class="text-danger required-asterik">*</span> Champs obligatoires
                    </div>
                </div>
            @endunless
        </div>
        {!! Form::close() !!}
    </div>
@endsection