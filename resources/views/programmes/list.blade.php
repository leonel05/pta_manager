@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Liste des programmes</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Liste des programmes')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                            <a href="{{ route('programmes.create') }}" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i>
                                Ajouter
                            </a>

                        {{--{!! Form::open(['route' => ['programmes.searchList'], 'method' => 'POST', 'class' => 'form-horizontal']) !!}
                            <div class="col-sm-5">
--}}{{--
                                {!! Form::select('code_pta', $pta_codes, $pta_code, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', 'required' => 'required', 'style' => 'witdh: 150px;']) !!}
--}}{{--
                                {!! Form::input('text', 'code_pta', null, ['class' => 'form-control typeahead', 'id' => 'txtPta', 'placeholder' => 'Saisir le code', 'required' => 'required', 'style' => 'witdh: 150px;']) !!}
                            </div>
                            <div class="col-sm-2">
                                <button type="submit" class="btn btn-primary">Filtrer par PTA</button>
                            </div>
                        {!! Form::close() !!}
                    </div>
                    <br>
                    <br>--}}
                    <hr>
                    <div class="table-responsive">
                        <table id="dataTable" class="table table-hover table-striped">
                            <thead>
                            <tr>
                                <th>Code PTA</th>
                                <th>Code</th>
                                <th>Libelle</th>
                                <th>Objectif Spécifique</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($programmes as $programme)
                                <tr>
                                    <td>{{ $programme->code_pta }}</td>
                                    <td>{{ $programme->code }}</td>
                                    <td>{{ $programme->libelle }}</td>
                                    <td>{{ $programme->objectif }}</td>
                                    <td>
                                        {{--<a href="{{ route('programmes.destroy', ['id' => $programme->id]) }}" class="btn-sm btn-danger"
                                           title="Supprimer"><i class="fa fa-trash-o"></i></a>--}}
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
@endsection