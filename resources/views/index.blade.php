@extends('layouts.app-content')

@section('page-title')
    Tableau de bord
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord')])
@endsection

@section('page-content')
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2">
                <div class="display">
                    <div class="number">
                        <h3 class="font-green-sharp">{{ $nbr_pta }}<small class="font-green-sharp" style="font-size: 12px;">PTA</small></h3>
                        <small>TOTAL DES PTA</small>
                    </div>
                    <div class="icon">
                        <i class="icon-calendar"></i>
                    </div>
                </div>
                <div class="progress-info">
                    <div class="progress">
								<span style="width: 100%;" class="progress-bar progress-bar-success green-sharp">
								<span class="sr-only"><a href="{{ route('pta.index') }}" class="btn btn-link"></a>Consulter</span>
								</span>
                    </div>
                    <div class="status">
                        <div class="status-title">
                            <a href="{{ route('pta.index') }}"><i class="fa fa-eye"></i> Consulter</a>
                        </div>
                        <div class="status-number">
                            <a href="{{ route('pta.create') }}">Ajouter <i class="fa fa-plus-circle"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2">
                <div class="display">
                    <div class="number">
                        <h3 class="font-yellow-gold">{{ $nbr_eval }}<small class="font-yellow-gold" style="font-size: 12px;">EVALUATIONS</small></h3>
                        <small>TABLEAUX D'EVALUATIONS</small>
                    </div>
                    <div class="icon">
                        <i class="icon-layers"></i>
                    </div>
                </div>
                <div class="progress-info">
                    <div class="progress">
								<span style="width: 100%;" class="progress-bar progress-bar-success yellow-gold">
								<span class="sr-only"><a href="{{ route('evaluations.create') }}" class="btn btn-link"><i class="fa fa-plus-circle"></i> Ajouter</a></span>
								</span>
                    </div>
                    <div class="status">
                        <div class="status-title">
                            <a href="{{ route('evaluations.index') }}"><i class="fa fa-eye"></i> Consulter</a>
                        </div>
                        <div class="status-number">
                            <a href="{{ route('evaluations.create') }}">Ajouter <i class="fa fa-plus-circle"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2">
                <div class="display">
                    <div class="number">
                        <h3 class="font-blue-sharp">{{ $nbr_rapport }}<small class="font-blue-sharp" style="font-size: 12px;">RAPPORTS</small></h3>
                        <small>TOTAL DES RAPPORTS</small>
                    </div>
                    <div class="icon">
                        <i class="glyphicon glyphicon-book"></i>
                    </div>
                </div>
                <div class="progress-info">
                    <div class="progress">
								<span style="width: 100%;" class="progress-bar progress-bar-success blue-sharp">
								<span class="sr-only"><a href="{{ route('rapports.index') }}" class="btn btn-link"><i class="fa fa-plus-circle"></i> Ajouter</a></span>
								</span>
                    </div>
                    <div class="status">
                        <div class="status-title">
                            <a href="{{ route('rapports.index') }}"><i class="fa fa-eye"></i> Consulter</a>
                        </div>
                        <div class="status-number">
                            <a href="{{ route('rapports.create') }}">Ajouter <i class="fa fa-plus-circle"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
            <div class="dashboard-stat2">
                <div class="display">
                    <div class="number">
                        <h3 class="font-red-soft"><small class="font-red-soft" style="font-size: 12px;">PARAMETRES</small></h3>
                        <small>Mises à jour !</small>
                    </div>
                    <div class="icon">
                        <i class="fa fa-cogs"></i>
                    </div>
                </div>
                <div class="progress-info">
                    <div class="progress">
								<span style="width: 100%;" class="progress-bar progress-bar-success red-soft">
								</span>
                    </div>
                    <div class="status">
                        <div class="status-title">
                            <a href="{{ route('ministeres.create') }}"><i class="fa fa-plus-circle"></i> Ministère</a>
                        </div>
                        <div class="status-number">
                            <a href="{{ route('structures.create') }}">Structure <i class="fa fa-plus-circle"></i> </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row margin-top-10">
        <div class="col-sm-6 col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="tools">
                        Importer un PTA PAR EXCEL
                    </div>
                    <hr>
                    @if ($message = Session::get('success'))

                        <div class="alert alert-success" role="alert">

                            {{ Session::get('success') }}

                        </div>

                    @endif

                    @if ($message = Session::get('error'))

                        <div class="alert alert-danger" role="alert">

                            {{ Session::get('error') }}

                        </div>

                    @endif
                    @if(isset($message))
                        <div class="col-sm-offset-2 col-sm-8 alert alert-{{ $type }}">
                            {{ $message }}
                        </div>
                    @endif

                    <h3>Importer un fichier Excel:</h3>

                    <form style="border: 1px solid #e1e1e1;margin-top: 15px;padding: 20px;" action="{{ URL::to('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">


                        <input type="file" name="import_file" />

                        {{ csrf_field() }}

                        <br/>


                        <button class="btn btn-primary">Import CSV ou Excel File</button>


                    </form>


                </div>
            </div>
        </div>
        <div class="col-sm-6 col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="tools">
                        Importer un PTA depuis SYASAPTA
                    </div>
                    <hr>
                    <h3>Importer un PTA:</h3>
                    {!! Form::open(['route' => 'pta.import', 'class' => 'form-horizontal', 'style' => 'border: 1px solid #e1e1e1;margin-top: 15px;padding: 20px;']) !!}

                    <div class="form-group">
{{--
                        {!! Form::label('pta', 'PTA', ['class' => 'col-xs-12']) !!}
--}}
                        <div class="col-xs-12">
                            {!! Form::select('pta', $list, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner un PTA']) !!}
                        </div>
                    </div>
                    {{--<div class="form-group required">
                        {!! Form::label('ministry', 'Ministère', ['class' => 'col-xs-12']) !!}
                        <div class="col-xs-12">
                            {!! Form::select('ministry', $ministeres, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner']) !!}
                        </div>
                    </div>--}}


                    <button type="submit" class="btn btn-primary">Importer le PTA</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection