@extends('layouts.app-content')

@section('page-title')
    Paramètres de base
    <small>Liste des ministères</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'Paramètres de base', 'Liste des ministères')])
@endsection


@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="tools">
                        <a href="{{ route('typestructures.create') }}" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i>
                            Ajouter
                        </a>
                    </div>
                    <hr>
                    <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Code</th>
                                <th>Type de structure</th>
                                <!--th>Actions</th-->
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($typestructures as $typestructure)
                                <?php $compteur = 1; ?>
                                <tr>
                                    <td>{{ $typestructure->id }}</td>
                                    <td>{{ $typestructure->code }}</td>
                                    <td>{{ $typestructure->libelle }}</td>
                                    <!--td>
                                        <a href="{{ route('typestructures.destroy', ['id' => $typestructure->id]) }}" class="btn-sm btn-danger"
                                           title="Supprimer"><i class="fa fa-trash-o"></i></a>
                                    </td-->
                                </tr>
                                <?php $compteur ++; ?>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection