@extends('layouts.app')

@section('content')
    <!-- BEGIN CONTAINER -->
    <div class="page-container">
    @include ('layouts.includes.sidebar')
    <!-- BEGIN CONTENT -->
        <div class="page-content-wrapper">
            <div class="page-content">
            <!-- BEGIN PAGE HEAD -->
                <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                        <h1>@yield('page-title')</h1>
                    </div>
                <!-- END PAGE TITLE -->
                </div>
            <!-- END PAGE HEAD -->
            <!-- BEGIN PAGE BREADCRUMB -->
                <div class="page-bar">
                    @yield('breadcrumb')
                </div>
                @yield('page-content')
            </div>
        </div>
        <!-- END CONTENT -->
    </div>
    <!-- END CONTAINER -->
@endsection