<ul class="page-breadcrumb breadcrumbs">
    @foreach($breadcrumb as $link)
        @if(!($link == $breadcrumb[sizeof($breadcrumb)-1]))
            <li>
                @if($link == $breadcrumb[0])
                    <i class="fa fa-home"></i>
                    &nbsp;
                @endif
                <a href="javascript:;">{{ $link }}</a>
                &nbsp;
                <i class="fa fa-angle-right"></i>
            </li>
            @else
                <li class="active">
                    @if($link == $breadcrumb[0])
                        <i class="fa fa-home"></i>
                    @endif
                    &nbsp;
                    {{ $link }}
                </li>
        @endif
    @endforeach
</ul>