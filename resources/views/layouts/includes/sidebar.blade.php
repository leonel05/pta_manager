<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
        <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
        <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
        <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
        <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
        <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
        <ul class="page-sidebar-menu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
            <li class="start active ">
                <a href="{{ route('home') }}">
                    <i class="icon-home"></i>
                    <span class="title">Tableau de Bord</span>
                </a>
            </li>
            <li>
                <a href="javascript:;">
                    <i class="glyphicon glyphicon-calendar"></i>
                    PTA
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="javascript:;">
                            <i class="icon-arrow-down"></i>
                            <span class="title">Importer un PTA</span>
                            <span class="arrow "></span>
                        </a>
                        <ul class="sub-menu">
                            <li>
                                <a href="{{ route('pta.importexcel') }}">
                                    <i class="fa fa-file-excel-o"></i>
                                    <span class="title">Depuis Excel</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('pta.formimport') }}">
                                    <i class="fa fa-database"></i>
                                    <span class="title">Depuis Syasapta</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="{{ route('pta.create') }}">
                            <i class="glyphicon glyphicon-plus"></i>
                            Nouveau PTA</a>
                    </li>
                    <li>
                        <a href="{{ route('pta.index') }}">
                            <i class="glyphicon glyphicon-list"></i>
                            Liste des PTA</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href="{{ route('programmes.index') }}">
                    <i class="glyphicon glyphicon-bookmark"></i>
                    Programmes</a>
            </li>
            <li>
                <a href="{{ route('objectifs.index') }}">
                    <i class="icon-target"></i>
                    Objectifs & Résultats</a>
            </li>
            <li>
                <a href="{{ route('actions.index') }}">
                    <i class="icon-tag"></i>
                    Actions</a>
            </li>
            <li>
                <a href="{{ route('activites.index') }}">
                    <i class="icon-handbag"></i>
                    Activités</a>
            </li>
            <li>
                <a href="{{ route('taches.index') }}">
                    <i class="glyphicon glyphicon-th-list"></i>
                    Tâches</a>
            </li>
            <li>
                <a href="{{ route('evaluations.index') }}">
                    <i class="icon-layers"></i>
                    Tableaux d'évaluation</a>
            </li>
            <li>
                <a href="{{ route('rapports.index') }}">
                    <i class="glyphicon glyphicon-book"></i>
                    Rapports d'évaluations</a>
            </li>

            {{--<li>
                <a href="javascript:;">
                    <i class="icon-book-open"></i>
                    <span class="title">Revues</span>
                </a>
            </li>--}}
            <li>
                <a href="javascript:;">
                    <i class="icon-settings"></i>
                    <span class="title">Paramètres</span>
                    <span class="arrow "></span>
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="{{ route('ministeres.index') }}">
                            <i class="icon-home"></i>
                            Ministères</a>
                    </li>
                    <li>
                        <a href="{{ route('typestructures.index') }}">
                            <i class="icon-list"></i>
                            Types de Structures</a>
                    </li>
                    <li>
                        <a href="{{ route('structures.index') }}">
                            <i class="icon-tag"></i>
                            Structures</a>
                    </li>
                </ul>
            </li>

            
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
</div>
<!-- END SIDEBAR -->