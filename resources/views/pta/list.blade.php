@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Liste des PTA</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Liste des PTA')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                        <div class="table-responsive">
                        <table id="dataTable" class="table table-hover table-bordered table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Exercice</th>
                                <th>Ministère</th>
                                <th>Structure</th>
                                <th>Objectif Global</th>
                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pta_list as $pta)
                                <?php $compteur = 1; ?>
                                <tr>
                                    <td>{{ $pta->id }}</td>
                                    <td>{{ $pta->exercice }}</td>
                                    <td>{{ $pta->code_ministere }}</td>
                                    <td>{{ $pta->code_structure }}</td>
                                    <td>{{ $pta->obj_global }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('pta.show', ['id' => $pta->id]) }}" class="btn-sm btn-success"
                                           title="Consulter"><i class="fa fa-eye"></i></a>
                                        {{--<a href="{{ route('pta.destroy', ['id' => $pta->id]) }}" class="btn-sm btn-danger"
                                           title="Supprimer"><i class="fa fa-trash-o"></i></a>--}}
                                    </td>
                                </tr>
                                <?php $compteur ++; ?>
                            @endforeach
                            </tbody>
                        </table>

                            {{--Modal for PTA importation--}}
{{--
                        <div class="modal fade" id="import-pta" tabindex="-1" aria-hidden="true" style="display: none;">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header bg-blue-dark">
                                        <button type="button" class="close" data-dismiss="modal"
                                                aria-hidden="true"></button>
                                        <h4 class="modal-title">Importation d'un PTA</h4>
                                    </div>
                                    {!! Form::open(['route' => 'pta.import', 'class' => 'form-horizontal']) !!}
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-xs-offset-1 col-xs-10">
                                                <div class="form-group">
                                                    {!! Form::label('years', 'Exercice', ['class' => 'col-xs-12']) !!}
                                                    <div class="col-xs-12">
                                                        {!! Form::select('years', $annees, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner']) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group required">
                                                    {!! Form::label('ministry', 'Ministère', ['class' => 'col-xs-12']) !!}
                                                    <div class="col-xs-12">
                                                        {!! Form::select('ministry', $ministeres, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner']) !!}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn default" data-dismiss="modal">Quitter</button>
                                        <button type="submit" class="btn blue">Importer</button>
                                    </div>
                                    {!! Form::close() !!}
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
--}}
                        {{--End Modal--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection