@extends('layouts.app-content')

@section('page-title')
    Programme de Travail Annuel
    <small>Aperçu d'un PTA</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Aperçu d\'un PTA')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="form-actions top margin-bottom-20">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="btn-group">
                                <a href="{{ route('pta.index') }}" class="btn btn-default ">
                                    <i class="fa fa-arrow-left"></i> Liste des PTA
                                </a>
                                {{--<a href="{{ route('rapports.edit', ['id' => $rapport->id]) }}" class="btn btn-primary">
                                    <i class="fa fa-edit"></i> Modifier
                                </a>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label><b>MINISTERE : &nbsp;</b></label> {{ $ministere }}
                        </div>
                        <div class="col-sm-6">
                            <label><b>STRUCTURE : &nbsp;</b></label> -
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <label><b>EXERCICE : &nbsp;</b></label> {{ $annee }}
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th rowspan="2">CODE</th>
                                <th rowspan="2">Objectifs(O) Actions(A) Activités(a) Taches(t)</th>
                                <th rowspan="2">MONTANT PROGRAMME <br> (F CFA)</th>
                                <th colspan="2">STRUCTURES</th>
                                <th rowspan="2">Période (Mois)</th>
                                <th>Poids Global (%)</th>
                            </tr>
                            <tr>
                                <td>RESP</td>
                                <td>ASS</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($programmes as $programme)
                                <tr style="background-color: #aaa;">
                                    <td>{{ $programme->code }} </td>
                                    <td colspan="6">{{ $programme->libelle.' : '.$programme->objectif }}</td>
                                </tr>
                                @foreach($objectifs[$programme->code] as $objectif)
                                    <tr style="font-weight: bold;">
                                        <td>{{ $objectif->abbrev }}</td>
                                        <td>{{ $objectif->libelle }}</td>
                                        <td>{{ $objectif->montant }}</td>
                                        <td>{{ $objectif->responsable }}</td>
                                        <td>{{ $objectif->associees }}</td>
                                        <td>{{ $objectif->nb_mois }}</td>
                                        <td>{{ $objectif->poids }}</td>
                                    </tr>
                                    <tr style="font-weight: bold; font-style: italic;">
                                        <td>R</td>
                                        <td>{{ $objectif->resultat }}</td>
                                        <td>{{ $objectif->montant }}</td>
                                        <td>{{ $objectif->responsable }}</td>
                                        <td>{{ $objectif->associees }}</td>
                                        <td>{{ $objectif->nb_mois }}</td>
                                        <td>{{ $objectif->poids }}</td>
                                    </tr>
                                        @foreach($actions[$objectif->code] as $action)
                                            <tr style="font-style: italic; color: orange;">
                                                <td>{{ $action->abbrev }}</td>
                                                <td>{{ $action->libelle }}</td>
                                                <td>{{ $action->montant }}</td>
                                                <td>{{ $objectif->responsable }}</td>
                                                <td>{{ $action->associees }}</td>
                                                <td>{{ $action->nb_mois }}</td>
                                                <td>{{ $action->poids }}</td>
                                            </tr>
                                            @foreach($activites[$action->code] as $activite)
                                                <tr style="font-style: italic; color: green;">
                                                    <td>{{ $activite->abbrev }}</td>
                                                    <td>{{ $activite->libelle }}</td>
                                                    <td>{{ $activite->montant }}</td>
                                                    <td>{{ $objectif->responsable }}</td>
                                                    <td>{{ $activite->associees }}</td>
                                                    <td>{{ $activite->nb_mois }}</td>
                                                    <td>{{ $activite->poids }}</td>
                                                </tr>
                                                @foreach($taches[$activite->code] as $tache)
                                                    <tr style="font-style: italic; color: #555;">
                                                        <td>{{ $tache->abbrev }}</td>
                                                        <td>{{ $tache->libelle }}</td>
                                                        <td>{{ $tache->montant }}</td>
                                                        <td>{{ $objectif->responsable }}</td>
                                                        <td>{{ $tache->associees }}</td>
                                                        <td>{{ $tache->nb_mois }}</td>
                                                        <td>{{ $tache->poids }}</td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        @endforeach
                                @endforeach
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection