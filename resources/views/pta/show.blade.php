@extends('layouts.app-content')

@section('page-title')
    Programme de Travail Annuel
    <small>Aperçu d'un PTA</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Aperçu d\'un PTA')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="form-actions top margin-bottom-20">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="btn-group">
                                <a href="{{ route('pta.index') }}" class="btn btn-default ">
                                    <i class="fa fa-arrow-left"></i> Liste des PTA
                                </a>
                                {{--<a href="{{ route('rapports.edit', ['id' => $rapport->id]) }}" class="btn btn-primary">
                                    <i class="fa fa-edit"></i> Modifier
                                </a>--}}
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-sm-6">
                            <label><b>MINISTERE : &nbsp;</b></label> {{ $ministere }}
                        </div>
                        <div class="col-sm-6">
                            <label><b>STRUCTURE : &nbsp;</b></label> -
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <label><b>EXERCICE : &nbsp;</b></label> {{ $annee }}
                        </div>
                    </div>
                    <br>
                    <div class="table-responsive">
                        <table class="table table-bordered text-center">
                            <thead>
                            <tr>
                                <th rowspan="2">CODE</th>
                                <th rowspan="2">Objectifs(O) Actions(A) Activités(a) Taches(t)</th>
                                <th rowspan="2">INDICATEURS</th>
                                <th colspan="3">MONTANT PROGRAMME <br> (F CFA)</th>
                                <th colspan="2">STRUCTURES</th>
                                <th rowspan="2">LOCALITES</th>
                                <th>Période prévue pour exécuter l'Activité</th>
                                <th rowspan="2">Pondération des Tâches (%)</th>
                                <th colspan="3">POIDS</th>
                            </tr>
                            <tr>
                                <td>BN</td>
                                <td>EXT</td>
                                <td>TOTAL</td>
                                <td>RESP</td>
                                <td>ASS</td>
                                <td>Période (Mois)</td>
                                <td>Financier</td>
                                <td>Temporel</td>
                                <td>Global (%)</td>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($programmes as $programme)
                                <tr style="background-color: #aaa;">
                                    <td>{{ $programme->codProgram }} </td>
                                    <td colspan="13">{{ $programme->libProgram.' : '.$programme->objSpecifiqProgram }}</td>
                                </tr>
                                @foreach($objectifs[$programme->codProgram] as $objectif)
                                    <tr style="font-weight: bold;">
                                        <td>{{ $objectif->codObjectif }}</td>
                                        <td>{{ $objectif->libObjectif }}</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                    @foreach($resultats[$objectif->codObjectif] as $resultat)
                                        <tr style="font-weight: bold; font-style: italic;">
                                            <td>{{ $resultat->codResultat }}</td>
                                            <td>{{ $resultat->libResultat }}</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        @foreach($actions[$resultat->codResultat] as $action)
                                            <tr style="font-style: italic; color: orange;">
                                                <td>{{ $action->codAction }}</td>
                                                <td>{{ $action->libAction }}</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>{{ $action->montant_engage }}</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>{{ $action->dateDebutAction.' - '.$action->dateFinAction }}</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                                <td>&nbsp;</td>
                                            </tr>
                                            @foreach($activites[$action->codAction] as $activite)
                                                <tr style="font-style: italic; color: green;">
                                                    <td>{{ $activite->codActivite }}</td>
                                                    <td>{{ $activite->libActivite }}</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>{{ $activite->montantActivite }}</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>{{ $activite->dbutPActivite.' - '.$activite->finPActivite }}</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>&nbsp;</td>
                                                    <td>{{ $activite->poidsActivite }}</td>
                                                </tr>
                                                @foreach($taches[$activite->codActivite] as $tache)
                                                    <tr style="font-style: italic; color: #555;">
                                                        <td>{{ $tache->codTache }}</td>
                                                        <td>{{ $tache->libTache }}</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>{{ $tache->montantprogramme }}</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>{{ $tache->dbutPTache.' - '.$tache->finPTache }}</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>&nbsp;</td>
                                                        <td>{{ $tache->poidsTache }}</td>
                                                    </tr>
                                                @endforeach
                                            @endforeach
                                        @endforeach
                                    @endforeach
                                @endforeach
                                <tr>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                    <td>&nbsp;</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection