<?php
$readonly = "";
$disabled = "";
$modification = false;
if ($pta->id) {  // Cas de la modification ou de la consultation
    if (isset($isView) && $isView) { // Cas de la consultation
        $options = ['class' => 'form-horizontal'];
        $readonly = "readonly";
    } else { // Cas de la modification
        $options = ['route' => ['pta.update', $pta->id], 'method' => 'PUT', 'class' => 'form-horizontal'];
        $modification = true;
    }
} else { // Cas de l'ajout
    $options = ['route' => ['pta.store'], 'method' => 'POST', 'class' => 'form-horizontal'];
    $disabled = "disabled";
}
?>

@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Création d'un PTA</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Création d\'un PTA')])
@endsection

@section('page-content')
    <div class="portlet light">
        {!! Form::model($pta, $options) !!}
        <div class="form-actions top margin-bottom-20">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="btn-group">
                        <a href="{{ route('pta.index') }}" class="btn btn-default">
                            <i class="fa fa-arrow-left"></i> Retour
                        </a>
                        @if(isset($isView) && $isView)
                            <a href="{{ route('pta.edit', ['id' => $pta->id]) }}" class="btn btn-primary">
                                <i class="fa fa-edit"></i> Modifier
                            </a>
                        @endif

                        {{--Cacher l'affichage du bouton enregistrer lors de la consultation--}}
                        @unless(isset($isView) && $isView)
                            <button type="submit" class="btn green"><i class="fa fa-save"></i> Enregistrer</button>
                        @endunless
                    </div>
                </div>
            </div>
        </div>
        <div class="form-body">
            @if(isset($message))
                <div class="col-sm-offset-2 col-sm-8 alert alert-{{ $type }}">
                    {{ $message }}
                </div>
            @endif
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    {!! Form::label('code_pta', 'Code du PTA', ['class' => 'text-primary label']) !!}
                    {!! Form::text('code_pta', null, ['class' => 'form-control', 'readonly']) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::label('code_ministere', 'Ministère', ['class' => 'text-primary label']) !!} <span
                            class="text-danger">*</span>
                    {!! Form::select('code_ministere', $ministeres, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', 'required' => 'required', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-4">
                    {!! Form::label('exercice', 'Année d\'exercice', ['class' => 'text-primary label']) !!} <span
                            class="text-danger">*</span>
                    {!! Form::select('exercice', ['2010' => '2010', '2011' => '2011', '2012' => '2012', '2013' => '2013', '2014' => '2014', '2015' => '2015', '2016' => '2016', '2017' => '2017', '2018' => '2018'], null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', 'required' => 'required', $readonly]) !!}
                </div>
                <div class="col-sm-4">
                    {!! Form::label('code_structure', 'Structure', ['class' => 'text-primary label']) !!}
                    {!! Form::select('code_structure', $structures, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner', $readonly]) !!}
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-8">
                    {!! Form::label('obj_global', 'Objectif Global', ['class' => 'text-primary label']) !!}
                    {!! Form::textarea('obj_global', null, ['class' => 'form-control', 'rows' => '2', $readonly]) !!}
                </div>
            </div>

            @unless(isset($isView) && $isView)
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <span class="text-danger required-asterik">*</span> Champs obligatoires
                    </div>
                </div>
            @endunless
        </div>
        {!! Form::close() !!}
    </div>
@endsection