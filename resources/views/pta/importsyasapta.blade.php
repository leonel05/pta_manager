@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Importation depuis SYASAPTA des PTA</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Importation depuis SYASAPTA des PTA')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="tools">
                        Importer un PTA depuis SYASAPTA
                    </div>
                    <hr>
                    <h3>Importer un PTA:</h3>
                    {!! Form::open(['route' => 'pta.import', 'class' => 'form-horizontal', 'style' => 'border: 1px solid #e1e1e1;margin-top: 15px;padding: 20px;']) !!}

                        <div class="form-group">
                            {!! Form::label('pta', 'PTA', ['class' => 'col-xs-12']) !!}
                            <div class="col-xs-12">
                                {!! Form::select('pta', $list, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner']) !!}
                            </div>
                        </div>
                        {{--<div class="form-group required">
                            {!! Form::label('ministry', 'Ministère', ['class' => 'col-xs-12']) !!}
                            <div class="col-xs-12">
                                {!! Form::select('ministry', $ministeres, null, ['class' => 'form-control select2', 'placeholder' => 'Veuillez selectionner']) !!}
                            </div>
                        </div>--}}

                        <br/>

                        <button type="submit" class="btn btn-primary">Importer le PTA</button>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection