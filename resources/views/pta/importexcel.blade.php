@extends('layouts.app-content')

@section('page-title')
    Programmes de Travaux Annuels
    <small>Importation par fichier Excel des PTA</small>
@endsection

@section('breadcrumb')
    @include ('layouts.includes.breadcrumb', ['breadcrumb' => array('Tableau de Bord', 'PTA', 'Importation par fichier Excel des PTA')])
@endsection

@section('page-content')
    <div class="row margin-top-10">
        <div class="col-xs-12">
            <!-- BEGIN PORTLET-->
            <div class="portlet light ">
                <div class="portlet-body">
                    <div class="tools">
                       Importer un PTA PAR EXCEL
                    </div>
                    <hr>
                    




                                    @if ($message = Session::get('success'))

                    <div class="alert alert-success" role="alert">

                        {{ Session::get('success') }}

                    </div>

                @endif


                @if ($message = Session::get('error'))

                    <div class="alert alert-danger" role="alert">

                        {{ Session::get('error') }}

                    </div>

                @endif
                @if(isset($message))
                <div class="col-sm-offset-2 col-sm-8 alert alert-{{ $type }}">
                    {{ $message }}
                </div>
            @endif

                <h3>Importer un fichier Excel:</h3>

                <form style="border: 1px solid #e1e1e1;margin-top: 15px;padding: 20px;" action="{{ URL::to('importExcel') }}" class="form-horizontal" method="post" enctype="multipart/form-data">


                    <input type="file" name="import_file" />

                    {{ csrf_field() }}

                    <br/>


                    <button class="btn btn-primary">Import CSV ou Excel File</button>


                </form>


                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
@endsection