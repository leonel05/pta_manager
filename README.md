# Gestion des Plans de Travail Annuel (PTA)

## Description

Il s'agit d'une plateforme de privé, permettant à certains services publics de mieux gèrer leurs Plans de Travail Annuel . Ainsi pour chaque plan créé sur la plateforme,on peut rajouter et gérer les objectifs, les tâches, les operatiins,etc.

## Technologies

**Langage de programmation:** PHP, Javascript

**Frameworks :** Laravel, Bootstrap

**Base de données:** MySQL


**Outils:** Git, Excel
