-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Sam 10 Février 2018 à 13:48
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `syasapta`
--

-- --------------------------------------------------------

--
-- Structure de la table `action`
--

CREATE TABLE `action` (
  `codAction` varchar(254) NOT NULL,
  `codSource` varchar(254) NOT NULL,
  `codResultat` varchar(254) DEFAULT NULL,
  `libAction` varchar(254) DEFAULT NULL,
  `montant_engage` int(11) NOT NULL,
  `dateDebutAction` int(254) NOT NULL,
  `dateFinAction` int(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `action`
--

INSERT INTO `action` (`codAction`, `codSource`, `codResultat`, `libAction`, `montant_engage`, `dateDebutAction`, `dateFinAction`) VALUES
('A111.1', 'BN', 'R11.1', 'Formation des SFD au respect de la réglementation', 22300, 1, 4),
('A121.1', 'BN', 'R12.1', 'Renforcement des capacités techniques des SFD', 976106, 1, 12),
('A121.2', 'BN', 'R12.1', 'Renforcement des capacités du réseau MCPP', 150000, 1, 12),
('A121.3', 'BN', 'R12.1', 'Appui au recouvrement des créances impayées par les clients des SFD ', 14000, 1, 12),
('A121.4', 'ACDI', 'R12.1', 'Appui au recouvrement des créances impayées par les clients des SFD ', 12500, 1, 12),
('A122.1', 'ACDI', 'R12.2', 'Opérationnalisation du CNM', 41000, 1, 12),
('A122.2', 'BN', 'R12.2', 'Opérationnalisation des structures en charge de la micro finance ', 1875984, 1, 12),
('A122.3', 'BN', 'R12.2', 'Achat d\'équipement au profit des SFD partenaires du FNM ', 140000, 1, 12);

-- --------------------------------------------------------

--
-- Structure de la table `activite`
--

CREATE TABLE `activite` (
  `codActivite` varchar(254) NOT NULL,
  `codAction` varchar(254) DEFAULT NULL,
  `libActivite` varchar(254) DEFAULT NULL,
  `dbutPActivite` int(80) DEFAULT NULL,
  `finPActivite` int(80) DEFAULT NULL,
  `poidsActivite` int(11) DEFAULT NULL,
  `montantActivite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `activite`
--

INSERT INTO `activite` (`codActivite`, `codAction`, `libActivite`, `dbutPActivite`, `finPActivite`, `poidsActivite`, `montantActivite`) VALUES
('ACT1111.1', 'A111.1', 'Atelier de sensibilisation des  Structures Financières opérant dans l\'informel sur la nécessité et l\'utilité de la formalisation (Deux regroupements dont un au Nord et un au Sud)', 1, 4, 31, 7000),
('ACT1111.2', 'A111.1', 'Conception et réalisation d\'un guide des usagers de la micro finance', 1, 3, 22, 5000),
('ACT1111.3', 'A111.1', 'Sensibilisation et communication pour la promotion des meilleurs comportements des usagers (par le biais des médias)', 1, 4, 31, 7000);

-- --------------------------------------------------------

--
-- Structure de la table `agent`
--

CREATE TABLE `agent` (
  `numAg` int(11) NOT NULL,
  `codStructure` varchar(254) DEFAULT NULL,
  `codPoste` varchar(254) NOT NULL,
  `NomAg` varchar(254) DEFAULT NULL,
  `prenomAg` varchar(254) DEFAULT NULL,
  `datNaisAg` date DEFAULT NULL,
  `sexeAg` varchar(254) DEFAULT NULL,
  `loginAg` varchar(254) DEFAULT NULL,
  `passwAg` varchar(254) DEFAULT NULL,
  `mailAg` varchar(254) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `agent`
--

INSERT INTO `agent` (`numAg`, `codStructure`, `codPoste`, `NomAg`, `prenomAg`, `datNaisAg`, `sexeAg`, `loginAg`, `passwAg`, `mailAg`) VALUES
(1, 'DDZC', 'PF', 'TENAKAH', 'Dyne', '2012-12-12', 'Masculin', 'dyne', 'dyne', 'mariidyne@yahoo.fr'),
(2, 'DPP', 'ADMIN', 'admin', 'admin', '2008-08-04', 'Masculin', 'admin', 'admin', 'admin@yahoo.fr'),
(3, 'DPMF', 'C/CSE', 'DAVID GNANHOUI', 'Franck', '2013-12-03', 'Masculin', 'franck', 'franck', 'lordfrancky@yahoo.fr'),
(4, 'DDZC', 'PF', 'AGOSSOU', 'Joseph', '1980-06-04', 'Masculin', 'joseph', 'joseph', 'joseph@yahoo.fr'),
(5, 'DPE', 'CF', 'MADOUGOU', 'Mouhamed', '1980-06-04', 'Masculin', 'mouhamed', 'mouhamed', 'mouhamed@yahoo.fr');

-- --------------------------------------------------------

--
-- Structure de la table `detail_rapport`
--

CREATE TABLE `detail_rapport` (
  `numRapport` int(11) NOT NULL,
  `codTache` varchar(254) NOT NULL,
  `montantEngage` int(11) DEFAULT NULL,
  `tauxRealisation` int(11) DEFAULT NULL,
  `observation` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `detail_rapport`
--

INSERT INTO `detail_rapport` (`numRapport`, `codTache`, `montantEngage`, `tauxRealisation`, `observation`) VALUES
(1, 'T11111.1', 0, 5, ''),
(1, 'T11112.1', 2000, 12, 'Retard');

-- --------------------------------------------------------

--
-- Structure de la table `exercice`
--

CREATE TABLE `exercice` (
  `anneeExerc` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `exercice`
--

INSERT INTO `exercice` (`anneeExerc`) VALUES
('2013');

-- --------------------------------------------------------

--
-- Structure de la table `ministere`
--

CREATE TABLE `ministere` (
  `codMinist` varchar(254) NOT NULL,
  `libMinist` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ministere`
--

INSERT INTO `ministere` (`codMinist`, `libMinist`) VALUES
('MAEIAFBE', 'Ministère des Affaires Etrangères, de l\'Intégration Africaine, de la Francophonie et des Béninois de l\'Extérieur'),
('MAEP', 'Ministère de l\'Agriculture et de la Pêche'),
('MCAAT', 'Ministère de la Culture, de l\'Alphabétisation, de l\'Artisanat et du Tourisme'),
('MCMEJF', 'Ministère Chargé de la Microfinance de l\'Emploi des Jeunes et des Femmes '),
('MCTIC', 'Ministère de la Communication et des Technologies de l\'Information et de la Communication'),
('MDAEP', 'Ministère du Développement, de l\'Analyse Economique et de la Prospective'),
('MDGLAAT', 'Ministère de la Décentralisation, de la Gouvernance Locale, de l\'Administration et de l\'Aménagement du Territoire '),
('MDN', 'Ministère de la Défense Nationale'),
('MECPDEPPCAG', 'Ministère d\'Etat Chargé de la Prospective, du Développement, de l\'Evaluation des Politiques Publiques et de la Coordination de l\'Action Gouvernementale'),
('MEF', 'Ministère de l\'Economie et des Finances'),
('MEHU', 'Ministère de l\'Environnement, de l\'Habitat et de l\'Urbanisme'),
('MJLDH', 'Ministère de la Jeunesse et des Loisirs');

-- --------------------------------------------------------

--
-- Structure de la table `objectif`
--

CREATE TABLE `objectif` (
  `codObjectif` varchar(254) NOT NULL,
  `codProgram` varchar(254) DEFAULT NULL,
  `codOrdr` varchar(254) DEFAULT NULL,
  `libObjectif` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `objectif`
--

INSERT INTO `objectif` (`codObjectif`, `codProgram`, `codOrdr`, `libObjectif`) VALUES
('O1.1', 'PROG1', NULL, 'Appuyer  l\'institutionnalisation des structures financières '),
('O1.2', 'PROG1', NULL, 'Renforcer les capacités techniques et financières des SFD'),
('O1.3', 'PROG1', NULL, 'Intégrer la microfinance  au secteur financier'),
('O1.4', 'PROG1', NULL, 'Promouvoir la finance inclusive'),
('O1.5', 'PROG1', '', ''),
('O2.1', 'PROG2', NULL, ' Généraliser  l’approche Développement Conduit par les Communautés'),
('O2.2', 'PROG2', NULL, ' Accroitre  la capacité organisationnelle et technique des couches les plus vulnérables'),
('O3.1', 'PROG3', NULL, 'consolider les acquis et favoriser la création de nouveau emploi'),
('O4.1', 'PROG4', NULL, ' Administration et Gestion des Services');

-- --------------------------------------------------------

--
-- Structure de la table `poste`
--

CREATE TABLE `poste` (
  `codPoste` varchar(254) NOT NULL,
  `libellePoste` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `poste`
--

INSERT INTO `poste` (`codPoste`, `libellePoste`) VALUES
('ade', 'academiques'),
('ADMIN', 'Administrateur Système'),
('C/CSE', 'Chef CSE'),
('CCSE', 'Cadre de la CSE'),
('CF', 'Chef de File'),
('PF', 'Point Focal');

-- --------------------------------------------------------

--
-- Structure de la table `profiluser`
--

CREATE TABLE `profiluser` (
  `codProf` int(11) NOT NULL,
  `libelleProf` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `profiluser`
--

INSERT INTO `profiluser` (`codProf`, `libelleProf`) VALUES
(1, 'Administratreur'),
(2, 'Utilisateur');

-- --------------------------------------------------------

--
-- Structure de la table `programme`
--

CREATE TABLE `programme` (
  `codProgram` varchar(254) NOT NULL,
  `anneeExerc` varchar(5) DEFAULT NULL,
  `codMinist` varchar(254) DEFAULT NULL,
  `libProgram` varchar(254) DEFAULT NULL,
  `objSpecifiqProgram` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `programme`
--

INSERT INTO `programme` (`codProgram`, `anneeExerc`, `codMinist`, `libProgram`, `objSpecifiqProgram`) VALUES
('PROG1', '2013', 'MCMEJF', 'DEVELOPPEMENT DE LA MICROFINANCE', 'Promouvoir une micro finance professionnelle, viable et intégrée au secteur financier'),
('PROG2', '2013', 'MCMEJF', 'PROMOTION DES INITIATIVES LOCALES', 'Contribuer efficacement à la création  de la richesse'),
('PROG3', '2013', 'MCMEJF', 'Promotion de l\'Emploi', 'Consolider les acquis et favoriser la création de nouveau emploi'),
('PROG4', '2013', 'MCMEJF', 'Administration et Gestion des Services', 'Améliorer la gestion des ressources du Ministère et à assurer la mise en oeuvre des stratégies du Ministère');

-- --------------------------------------------------------

--
-- Structure de la table `pta`
--

CREATE TABLE `pta` (
  `anneeExerc` varchar(5) NOT NULL,
  `codMinist` varchar(254) NOT NULL,
  `codPta` varchar(254) DEFAULT NULL,
  `objGlobal` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `pta`
--

INSERT INTO `pta` (`anneeExerc`, `codMinist`, `codPta`, `objGlobal`) VALUES
('2013', 'MCMEJF', 'PTA2013', 'Développement');

-- --------------------------------------------------------

--
-- Structure de la table `rapport`
--

CREATE TABLE `rapport` (
  `numRapport` int(11) NOT NULL,
  `numAg` int(11) DEFAULT NULL,
  `codTypeRa` varchar(254) DEFAULT NULL,
  `libRapport` varchar(254) DEFAULT NULL,
  `dateRapport` date DEFAULT NULL,
  `periode` varchar(254) DEFAULT NULL,
  `envoye` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `rapport`
--

INSERT INTO `rapport` (`numRapport`, `numAg`, `codTypeRa`, `libRapport`, `dateRapport`, `periode`, `envoye`) VALUES
(1, 1, 'Mens', 'Rapport Mensuel au 31 janvier 2013', '2013-12-04', '1', 1),
(2, 1, 'Mens', 'Rapport Mensuel au 29 février 2013', '2013-12-07', '11', 1);

-- --------------------------------------------------------

--
-- Structure de la table `resultat`
--

CREATE TABLE `resultat` (
  `codResultat` varchar(254) NOT NULL,
  `codObjectif` varchar(254) DEFAULT NULL,
  `libResultat` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `resultat`
--

INSERT INTO `resultat` (`codResultat`, `codObjectif`, `libResultat`) VALUES
('41.4', 'O4.1', 'Les documents de planification du ministère sont mieux élaborés, mis en œuvre et évalués'),
('R11.1', 'O1.1', 'Les structures financières opérant dans l\'informel sont institutionnalisées'),
('R12.1', 'O1.2', 'Les capacités techniques et financières des SFD sont renforcées'),
('R12.2', 'O1.2', 'Les capacités des structures de promotion de la micro finance sont opérationnelles et renforcées'),
('R13.1', 'O1.3', 'Les SFD sont mieux intégrés au système financier '),
('R13.2', 'O1.3', 'Les banques sont appuyées pour la promotion de nouveaux produits financiers à l\'endroit des SFD'),
('R14.1', 'O1.4', 'Les activités économiques en milieu rural sont financées'),
('R14.2', 'O1.4', 'Des mesures incitatives sont initiées pour favoriser l’extension ou l’installation des SFD en milieu rural '),
('R14.3', 'O1.4', 'Des lignes de crédit sont mises en place pour le refinancement des SFD'),
('R21.1', 'O2.1', 'L\'approche Développement Conduit par les Communautés (DCC) est appropriée par les acteurs à la base'),
('R22.1', 'O2.2', 'Les créneaux porteurs à la base sont promus'),
('R31.1', 'O3.1', 'Les initiatives en faveur de l\'emploi salarié sont mises en œuvre'),
('R31.2', 'O3.1', 'Les initiatives en faveur de l\'auto emploi sont assurées'),
('R31.3', 'O3.1', 'Les structures en charge de l\'emploi sont fonctionnelles '),
('R41.1', 'O4.1', 'Les Ressources humaines sont performantes'),
('R41.2', 'O4.1', 'Les ressources financières et matérielles sont mieux mobilisées'),
('R41.3', 'O4.1', 'Les Ressources financières et matérielles allouées au Ministère sont mieux gérées');

-- --------------------------------------------------------

--
-- Structure de la table `revue`
--

CREATE TABLE `revue` (
  `numRevue` int(11) NOT NULL,
  `datRevue` date DEFAULT NULL,
  `libRevue` varchar(254) DEFAULT NULL,
  `numAg` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `revue`
--

INSERT INTO `revue` (`numRevue`, `datRevue`, `libRevue`, `numAg`) VALUES
(1, '2013-12-30', 'Atelier de validation du PTA 2014.', 1),
(2, '2017-05-04', 'Première Revue Trismestrielle 2017', 1);

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

CREATE TABLE `role` (
  `codStructure` varchar(254) NOT NULL,
  `codAction` varchar(254) NOT NULL,
  `Responsable` tinyint(1) DEFAULT NULL,
  `Associee` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `session`
--

CREATE TABLE `session` (
  `numSess` int(11) NOT NULL,
  `numAg` int(11) DEFAULT NULL,
  `heureDebutSession` datetime DEFAULT NULL,
  `heureFinSession` datetime DEFAULT NULL,
  `dateSession` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `session`
--

INSERT INTO `session` (`numSess`, `numAg`, `heureDebutSession`, `heureFinSession`, `dateSession`) VALUES
(1, 2, '2013-12-25 08:08:41', '2013-12-25 08:08:41', '2013-12-25 08:08:41'),
(2, 2, '2013-12-25 09:10:48', '2013-12-25 09:10:48', '2013-12-25 09:10:48'),
(3, 2, '2013-12-25 09:22:13', '2013-12-25 09:22:13', '2013-12-25 09:22:13'),
(4, 2, '2013-12-25 09:37:54', '2013-12-25 09:37:54', '2013-12-25 09:37:54'),
(5, 2, '2013-12-25 09:42:13', '2013-12-25 09:42:13', '2013-12-25 09:42:13'),
(6, 2, '2013-12-25 12:11:47', '2013-12-25 12:11:47', '2013-12-25 12:11:47'),
(7, 2, '2013-12-25 13:39:50', '2013-12-25 13:39:50', '2013-12-25 13:39:50'),
(8, 3, '2013-12-25 13:49:24', '2013-12-25 13:49:24', '2013-12-25 13:49:24'),
(9, 1, '2013-12-25 13:57:13', '2013-12-25 13:57:13', '2013-12-25 13:57:13'),
(10, 2, '2013-12-25 14:07:02', '2013-12-25 14:07:02', '2013-12-25 14:07:02'),
(11, 3, '2013-12-25 14:21:17', '2013-12-25 14:21:17', '2013-12-25 14:21:17'),
(12, 2, '2013-12-25 14:21:58', '2013-12-25 14:21:58', '2013-12-25 14:21:58'),
(13, 2, '2013-12-25 14:28:27', '2013-12-25 14:28:27', '2013-12-25 14:28:27'),
(14, 2, '2013-12-25 14:41:16', '2013-12-25 14:41:16', '2013-12-25 14:41:16'),
(15, 2, '2013-12-25 14:50:25', '2013-12-25 14:50:25', '2013-12-25 14:50:25'),
(16, 2, '2013-12-25 15:14:50', '2013-12-25 15:14:50', '2013-12-25 15:14:50'),
(17, 2, '2013-12-25 15:19:54', '2013-12-25 15:19:54', '2013-12-25 15:19:54'),
(18, 2, '2013-12-25 15:23:31', '2013-12-25 15:23:31', '2013-12-25 15:23:31'),
(19, 2, '2013-12-25 15:25:09', '2013-12-25 15:25:09', '2013-12-25 15:25:09'),
(20, 2, '2013-12-25 15:30:49', '2013-12-25 15:30:49', '2013-12-25 15:30:49'),
(21, 2, '2013-12-25 15:55:34', '2013-12-25 15:55:34', '2013-12-25 15:55:34'),
(22, 2, '2013-12-25 15:57:03', '2013-12-25 15:57:03', '2013-12-25 15:57:03'),
(23, 2, '2013-12-25 15:59:24', '2013-12-25 15:59:24', '2013-12-25 15:59:24'),
(24, 2, '2013-12-25 16:00:58', '2013-12-25 16:00:58', '2013-12-25 16:00:58'),
(25, 2, '2013-12-25 18:32:29', '2013-12-25 18:32:29', '2013-12-25 18:32:29'),
(26, 2, '2013-12-25 18:45:06', '2013-12-25 18:45:06', '2013-12-25 18:45:06'),
(27, 2, '2013-12-25 19:41:34', '2013-12-25 19:41:34', '2013-12-25 19:41:34'),
(28, 2, '2013-12-26 02:42:34', '2013-12-26 02:42:34', '2013-12-26 02:42:34'),
(29, 1, '2013-12-26 03:40:44', '2013-12-26 03:40:44', '2013-12-26 03:40:44'),
(30, 2, '2013-12-26 03:56:41', '2013-12-26 03:56:41', '2013-12-26 03:56:41'),
(31, 1, '2013-12-26 03:57:11', '2013-12-26 03:57:11', '2013-12-26 03:57:11'),
(32, 1, '2013-12-26 04:00:27', '2013-12-26 04:00:27', '2013-12-26 04:00:27'),
(33, 2, '2013-12-26 04:04:13', '2013-12-26 04:04:13', '2013-12-26 04:04:13'),
(34, 1, '2013-12-26 04:04:28', '2013-12-26 04:04:28', '2013-12-26 04:04:28'),
(35, 1, '2013-12-26 04:07:06', '2013-12-26 04:07:06', '2013-12-26 04:07:06'),
(36, 2, '2013-12-26 04:07:52', '2013-12-26 04:07:52', '2013-12-26 04:07:52'),
(37, 2, '2013-12-26 04:12:13', '2013-12-26 04:12:13', '2013-12-26 04:12:13'),
(38, 1, '2013-12-26 04:12:24', '2013-12-26 04:12:24', '2013-12-26 04:12:24'),
(39, 1, '2013-12-26 04:18:15', '2013-12-26 04:18:15', '2013-12-26 04:18:15'),
(40, 1, '2013-12-26 04:22:45', '2013-12-26 04:22:45', '2013-12-26 04:22:45'),
(41, 2, '2013-12-26 04:31:26', '2013-12-26 04:31:26', '2013-12-26 04:31:26'),
(42, 1, '2013-12-26 04:31:43', '2013-12-26 04:31:43', '2013-12-26 04:31:43'),
(43, 1, '2013-12-26 05:32:01', '2013-12-26 05:32:01', '2013-12-26 05:32:01'),
(44, 2, '2013-12-26 05:35:37', '2013-12-26 05:35:37', '2013-12-26 05:35:37'),
(45, 1, '2013-12-26 05:35:47', '2013-12-26 05:35:47', '2013-12-26 05:35:47'),
(46, 2, '2013-12-26 13:21:42', '2013-12-26 13:21:42', '2013-12-26 13:21:42'),
(47, 1, '2013-12-26 13:22:14', '2013-12-26 13:22:14', '2013-12-26 13:22:14'),
(48, 2, '2013-12-26 13:25:01', '2013-12-26 13:25:01', '2013-12-26 13:25:01'),
(49, 1, '2013-12-26 13:25:23', '2013-12-26 13:25:23', '2013-12-26 13:25:23'),
(50, 2, '2013-12-26 13:55:55', '2013-12-26 13:55:55', '2013-12-26 13:55:55'),
(51, 2, '2013-12-26 13:56:44', '2013-12-26 13:56:44', '2013-12-26 13:56:44'),
(52, 1, '2013-12-26 13:57:01', '2013-12-26 13:57:01', '2013-12-26 13:57:01'),
(53, 2, '2013-12-26 14:19:02', '2013-12-26 14:19:02', '2013-12-26 14:19:02'),
(54, 2, '2013-12-26 14:19:21', '2013-12-26 14:19:21', '2013-12-26 14:19:21'),
(55, 1, '2013-12-26 14:19:53', '2013-12-26 14:19:53', '2013-12-26 14:19:53'),
(56, 2, '2013-12-26 14:29:42', '2013-12-26 14:29:42', '2013-12-26 14:29:42'),
(57, 1, '2013-12-26 14:30:08', '2013-12-26 14:30:08', '2013-12-26 14:30:08'),
(58, 2, '2013-12-26 14:38:20', '2013-12-26 14:38:20', '2013-12-26 14:38:20'),
(59, 2, '2013-12-26 14:40:04', '2013-12-26 14:40:04', '2013-12-26 14:40:04'),
(60, 2, '2013-12-26 16:22:19', '2013-12-26 16:22:19', '2013-12-26 16:22:19'),
(61, 2, '2013-12-26 16:29:14', '2013-12-26 16:29:14', '2013-12-26 16:29:14'),
(62, 1, '2013-12-26 16:32:09', '2013-12-26 16:32:09', '2013-12-26 16:32:09'),
(63, 2, '2013-12-26 16:39:41', '2013-12-26 16:39:41', '2013-12-26 16:39:41'),
(64, 2, '2013-12-26 16:51:30', '2013-12-26 16:51:30', '2013-12-26 16:51:30'),
(65, 2, '2013-12-26 16:52:53', '2013-12-26 16:52:53', '2013-12-26 16:52:53'),
(66, 2, '2013-12-26 16:57:02', '2013-12-26 16:57:02', '2013-12-26 16:57:02'),
(67, 2, '2013-12-26 17:01:47', '2013-12-26 17:01:47', '2013-12-26 17:01:47'),
(68, 1, '2013-12-26 17:03:20', '2013-12-26 17:03:20', '2013-12-26 17:03:20'),
(69, 3, '2013-12-26 17:33:23', '2013-12-26 17:33:23', '2013-12-26 17:33:23'),
(70, 1, '2013-12-26 17:34:21', '2013-12-26 17:34:21', '2013-12-26 17:34:21'),
(71, 2, '2013-12-26 17:36:43', '2013-12-26 17:36:43', '2013-12-26 17:36:43'),
(72, 2, '2013-12-26 17:39:29', '2013-12-26 17:39:30', '2013-12-26 17:39:29'),
(73, 2, '2013-12-26 17:40:36', '2013-12-26 17:41:18', '2013-12-26 17:40:36'),
(74, 2, '2013-12-26 17:43:08', '2013-12-26 17:43:44', '2013-12-26 17:43:08'),
(75, 2, '2013-12-26 20:15:56', '2013-12-26 20:16:29', '2013-12-26 20:15:56'),
(76, 2, '2013-12-26 20:17:24', '2013-12-26 20:17:24', '2013-12-26 20:17:24'),
(77, 2, '2017-04-21 09:23:38', '2017-04-21 09:23:46', '2017-04-21 09:23:38'),
(78, 2, '2017-04-21 09:36:16', '2017-04-21 09:38:38', '2017-04-21 09:36:16'),
(79, 1, '2017-04-21 09:38:47', '2017-04-21 09:41:16', '2017-04-21 09:38:47'),
(80, 2, '2017-04-21 10:10:01', '2017-04-21 10:13:58', '2017-04-21 10:10:01'),
(81, 2, '2017-04-28 18:49:10', '2017-04-28 18:54:38', '2017-04-28 18:49:10'),
(82, 2, '2017-05-01 15:31:36', '2017-05-01 15:32:10', '2017-05-01 15:31:36'),
(83, 2, '2017-05-03 13:54:21', '2017-05-03 13:55:52', '2017-05-03 13:54:21'),
(84, 2, '2017-05-04 10:24:52', '2017-05-04 10:28:48', '2017-05-04 10:24:52'),
(85, 2, '2017-07-01 16:04:05', '2017-07-08 17:06:45', '2017-07-01 16:04:05'),
(86, 2, '2017-07-09 13:45:22', '2017-07-09 14:00:31', '2017-07-09 13:45:22');

-- --------------------------------------------------------

--
-- Structure de la table `source`
--

CREATE TABLE `source` (
  `codSource` varchar(254) NOT NULL,
  `designSource` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `source`
--

INSERT INTO `source` (`codSource`, `designSource`) VALUES
('ACDI', NULL),
('BN', 'Budjet National');

-- --------------------------------------------------------

--
-- Structure de la table `structure`
--

CREATE TABLE `structure` (
  `codStructure` varchar(254) NOT NULL,
  `codTypeStruc` varchar(254) DEFAULT NULL,
  `codProgram` varchar(254) DEFAULT NULL,
  `codMinist` varchar(254) DEFAULT NULL,
  `libStructure` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `structure`
--

INSERT INTO `structure` (`codStructure`, `codTypeStruc`, `codProgram`, `codMinist`, `libStructure`) VALUES
('AGeFIB', 'OPS', 'PROG2', 'MCMEJF', ''),
('ANPE', 'OPS', 'PROG3', 'MCMEJF', 'Agence Nationale Pour l\'Emploi'),
('BPC', 'OPS', 'PROG2', 'MCMEJF', 'Business Promotion Center'),
('CCMP', 'DC', 'PROG4', 'MCMEJF', 'Cellule de Contrôle des Marchés Publics'),
('CNRC', 'DT', 'PROG1', 'MCMEJF', 'Commision Nationale de Recouvrement des Crédits'),
('DDZC', 'CM', 'PROG1', 'MCMEJF', 'Direction Départementale Zou-Collines'),
('DPE', 'DT', 'PROG3', 'MCMEJF', 'Direction de la Promotion de l\'Emploi'),
('DPMF', 'DT', 'PROG1', 'MCMEJF', 'Direction de la Promotion de la Microfinance'),
('DPP', 'DC', 'PROG4', 'MCMEJF', 'Direction  de la Programmation et de la Prospective'),
('DRH', 'DC', 'PROG4', 'MCMEJF', 'Direction des Ressources Humaines'),
('FNM', 'OPS', 'PROG1', 'MCMEJF', 'Fonds National de la Microfinance'),
('FNPEEJ', 'OPS', 'PROG3', 'MCMEJF', ''),
('PADIEB', 'CM', 'PROG1', 'MCMEJF', 'Projet d\'Appui au Développement des Initiatives '),
('PASEF', 'OPS', 'PROG2', 'MCMEJF', ''),
('UFLS', 'OPS', 'PROG4', 'MCMEJF', 'Unité Focale deLutte contre le Sida');

-- --------------------------------------------------------

--
-- Structure de la table `tache`
--

CREATE TABLE `tache` (
  `codTache` varchar(254) NOT NULL,
  `codActivite` varchar(254) DEFAULT NULL,
  `libTache` varchar(254) DEFAULT NULL,
  `dbutPTache` int(11) DEFAULT NULL,
  `finPTache` int(11) DEFAULT NULL,
  `montantprogramme` int(11) DEFAULT NULL,
  `poidsTache` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `tache`
--

INSERT INTO `tache` (`codTache`, `codActivite`, `libTache`, `dbutPTache`, `finPTache`, `montantprogramme`, `poidsTache`) VALUES
('T11111.1', 'ACT1111.1', 'Elaboration des TDR', 1, 3, 0, 10),
('T11111.2', 'ACT1111.1', 'Réalisation des séances  sensibilisation en deux regroupements dont un au Nord et un au Sud', 2, 3, 7000, 90),
('T11112.1', 'ACT1111.2', 'Elaboration des TDR', 3, 5, 0, 10),
('T11112.2', 'ACT1111.2', 'Recrutement des prestataires', 4, 5, 0, 10),
('T11112.3', 'ACT1111.2', 'Edition et reception de 2000 exemplaires du guide', 3, 5, 5000, 30),
('T11112.4', 'ACT1111.2', 'Diffusion du guide des usagers', 5, 6, 0, 50),
('T11113.1', 'ACT1111.3', 'Elaboration des TDR', 6, 6, 0, 10),
('T11113.2', 'ACT1111.3', 'Conception des messages, spots publicitaires, sketchs et émissions', 7, 9, 2000, 25),
('T11113.3', 'ACT1111.3', 'Sélection d’agences de communication et signature du contrat', 8, 10, 0, 15),
('T11113.4', 'ACT1111.3', 'Diffusion sur les médias (radios, télévisions)', 9, 11, 5000, 50);

-- --------------------------------------------------------

--
-- Structure de la table `type_rapport`
--

CREATE TABLE `type_rapport` (
  `codTypeRa` varchar(254) NOT NULL,
  `libTypeRa` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `type_rapport`
--

INSERT INTO `type_rapport` (`codTypeRa`, `libTypeRa`) VALUES
('Ann', 'Annuel'),
('Mens', 'Mensuel'),
('Sem', 'Semestriel'),
('Trim', 'Trimestriel');

-- --------------------------------------------------------

--
-- Structure de la table `type_structure`
--

CREATE TABLE `type_structure` (
  `codTypeStruc` varchar(254) NOT NULL,
  `libTypeStruc` varchar(254) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `type_structure`
--

INSERT INTO `type_structure` (`codTypeStruc`, `libTypeStruc`) VALUES
('CM', 'Cabinet du Ministre'),
('DC', 'Direction Centrale'),
('DD', 'Direction Départementale'),
('DT', 'Direction Technique'),
('OPS', 'Organismes, Projets, Tructures sous Tutelles'),
('SGM', 'Sécrétariat Général du Ministère');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateurs`
--

CREATE TABLE `utilisateurs` (
  `numU` int(11) NOT NULL,
  `nomU` varchar(254) DEFAULT NULL,
  `prenU` varchar(254) DEFAULT NULL,
  `loginU` varchar(254) DEFAULT NULL,
  `passU` varchar(254) DEFAULT NULL,
  `codProf` int(11) DEFAULT NULL,
  `emailU` varchar(60) DEFAULT NULL,
  `telU` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `utilisateurs`
--

INSERT INTO `utilisateurs` (`numU`, `nomU`, `prenU`, `loginU`, `passU`, `codProf`, `emailU`, `telU`) VALUES
(1, 'DAVID GNANHOUI', 'Gildas Franck', 'admin', 'admin', 1, 'admin@yahoo.fr', '21545445'),
(2, 'bobo', 'bibi', 'fifi', 'popo', 1, 'dodo@ya', '1455'),
(4, 'gogo', 'fofo', 'logo', 'diff', 1, 'coco@yahoo.fr', 'dof');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`codAction`),
  ADD KEY `FK_Association_24` (`codSource`),
  ADD KEY `FK_Association_35` (`codResultat`);

--
-- Index pour la table `activite`
--
ALTER TABLE `activite`
  ADD PRIMARY KEY (`codActivite`),
  ADD KEY `FK_Association_36` (`codAction`);

--
-- Index pour la table `agent`
--
ALTER TABLE `agent`
  ADD PRIMARY KEY (`numAg`),
  ADD KEY `FK_Association_15` (`codPoste`),
  ADD KEY `FK_Association_20` (`codStructure`);

--
-- Index pour la table `detail_rapport`
--
ALTER TABLE `detail_rapport`
  ADD PRIMARY KEY (`numRapport`,`codTache`);

--
-- Index pour la table `exercice`
--
ALTER TABLE `exercice`
  ADD PRIMARY KEY (`anneeExerc`);

--
-- Index pour la table `ministere`
--
ALTER TABLE `ministere`
  ADD PRIMARY KEY (`codMinist`);

--
-- Index pour la table `objectif`
--
ALTER TABLE `objectif`
  ADD PRIMARY KEY (`codObjectif`),
  ADD KEY `FK_Association_6` (`codProgram`);

--
-- Index pour la table `poste`
--
ALTER TABLE `poste`
  ADD PRIMARY KEY (`codPoste`),
  ADD KEY `AK_Identifiant_1` (`codPoste`);

--
-- Index pour la table `profiluser`
--
ALTER TABLE `profiluser`
  ADD PRIMARY KEY (`codProf`);

--
-- Index pour la table `programme`
--
ALTER TABLE `programme`
  ADD PRIMARY KEY (`codProgram`),
  ADD KEY `FK_Association_4` (`anneeExerc`),
  ADD KEY `FK_Association_5` (`codMinist`);

--
-- Index pour la table `pta`
--
ALTER TABLE `pta`
  ADD PRIMARY KEY (`anneeExerc`,`codMinist`),
  ADD KEY `FK_Association_1` (`anneeExerc`),
  ADD KEY `FK_Association_2` (`codMinist`);

--
-- Index pour la table `rapport`
--
ALTER TABLE `rapport`
  ADD PRIMARY KEY (`numRapport`),
  ADD KEY `AK_Identifiant_1` (`numRapport`);

--
-- Index pour la table `resultat`
--
ALTER TABLE `resultat`
  ADD PRIMARY KEY (`codResultat`),
  ADD KEY `FK_Identifiant_7` (`codObjectif`);

--
-- Index pour la table `revue`
--
ALTER TABLE `revue`
  ADD PRIMARY KEY (`numRevue`),
  ADD KEY `FK_Association_37` (`numAg`);

--
-- Index pour la table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`codStructure`,`codAction`);

--
-- Index pour la table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`numSess`);

--
-- Index pour la table `source`
--
ALTER TABLE `source`
  ADD PRIMARY KEY (`codSource`);

--
-- Index pour la table `structure`
--
ALTER TABLE `structure`
  ADD PRIMARY KEY (`codStructure`),
  ADD KEY `FK_Identifiant_9` (`codTypeStruc`),
  ADD KEY `FK_Identifiant_10` (`codProgram`),
  ADD KEY `FK_Identifiant_11` (`codMinist`);

--
-- Index pour la table `tache`
--
ALTER TABLE `tache`
  ADD PRIMARY KEY (`codTache`),
  ADD KEY `AK_Identifiant_8` (`codTache`),
  ADD KEY `FK_Association_8` (`codActivite`);

--
-- Index pour la table `type_rapport`
--
ALTER TABLE `type_rapport`
  ADD PRIMARY KEY (`codTypeRa`);

--
-- Index pour la table `type_structure`
--
ALTER TABLE `type_structure`
  ADD PRIMARY KEY (`codTypeStruc`);

--
-- Index pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  ADD PRIMARY KEY (`numU`),
  ADD KEY `FK_association4` (`codProf`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `agent`
--
ALTER TABLE `agent`
  MODIFY `numAg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `profiluser`
--
ALTER TABLE `profiluser`
  MODIFY `codProf` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `rapport`
--
ALTER TABLE `rapport`
  MODIFY `numRapport` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `revue`
--
ALTER TABLE `revue`
  MODIFY `numRevue` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT pour la table `session`
--
ALTER TABLE `session`
  MODIFY `numSess` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;
--
-- AUTO_INCREMENT pour la table `utilisateurs`
--
ALTER TABLE `utilisateurs`
  MODIFY `numU` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `action`
--
ALTER TABLE `action`
  ADD CONSTRAINT `FK_Association_24` FOREIGN KEY (`codSource`) REFERENCES `source` (`codSource`),
  ADD CONSTRAINT `FK_Association_35` FOREIGN KEY (`codResultat`) REFERENCES `resultat` (`codResultat`);

--
-- Contraintes pour la table `activite`
--
ALTER TABLE `activite`
  ADD CONSTRAINT `FK_Association_36` FOREIGN KEY (`codAction`) REFERENCES `action` (`codAction`);

--
-- Contraintes pour la table `agent`
--
ALTER TABLE `agent`
  ADD CONSTRAINT `FK_Association_15` FOREIGN KEY (`codPoste`) REFERENCES `poste` (`codPoste`),
  ADD CONSTRAINT `FK_Association_20` FOREIGN KEY (`codStructure`) REFERENCES `structure` (`codStructure`);

--
-- Contraintes pour la table `detail_rapport`
--
ALTER TABLE `detail_rapport`
  ADD CONSTRAINT `FK_Association_22` FOREIGN KEY (`numRapport`) REFERENCES `rapport` (`numRapport`);

--
-- Contraintes pour la table `objectif`
--
ALTER TABLE `objectif`
  ADD CONSTRAINT `FK_Association_6` FOREIGN KEY (`codProgram`) REFERENCES `programme` (`codProgram`);

--
-- Contraintes pour la table `programme`
--
ALTER TABLE `programme`
  ADD CONSTRAINT `FK_Association_4` FOREIGN KEY (`anneeExerc`) REFERENCES `exercice` (`anneeExerc`),
  ADD CONSTRAINT `FK_Association_5` FOREIGN KEY (`codMinist`) REFERENCES `ministere` (`codMinist`);

--
-- Contraintes pour la table `pta`
--
ALTER TABLE `pta`
  ADD CONSTRAINT `FK_Association_1` FOREIGN KEY (`anneeExerc`) REFERENCES `exercice` (`anneeExerc`),
  ADD CONSTRAINT `FK_Association_2` FOREIGN KEY (`codMinist`) REFERENCES `ministere` (`codMinist`);

--
-- Contraintes pour la table `resultat`
--
ALTER TABLE `resultat`
  ADD CONSTRAINT `FK_Association_7` FOREIGN KEY (`codObjectif`) REFERENCES `objectif` (`codObjectif`);

--
-- Contraintes pour la table `revue`
--
ALTER TABLE `revue`
  ADD CONSTRAINT `FK_Association_37` FOREIGN KEY (`numAg`) REFERENCES `agent` (`numAg`);

--
-- Contraintes pour la table `structure`
--
ALTER TABLE `structure`
  ADD CONSTRAINT `FK_Association_10` FOREIGN KEY (`codProgram`) REFERENCES `programme` (`codProgram`),
  ADD CONSTRAINT `FK_Association_11` FOREIGN KEY (`codMinist`) REFERENCES `ministere` (`codMinist`),
  ADD CONSTRAINT `FK_Association_9` FOREIGN KEY (`codTypeStruc`) REFERENCES `type_structure` (`codTypeStruc`);

--
-- Contraintes pour la table `tache`
--
ALTER TABLE `tache`
  ADD CONSTRAINT `FK_Association_8` FOREIGN KEY (`codActivite`) REFERENCES `activite` (`codActivite`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
