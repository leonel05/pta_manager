<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('home');

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
  /* PTA Routes */
  Route::get('/pta/importsyasapta', 'PtaController@getImportForm')->name('pta.formimport');
  Route::post('/pta/import', 'PtaController@import')->name('pta.import');
  Route::post('/pta/autocomplete', 'PtaController@getPtaInList')->name('pta.autocomplete');
  Route::post('/programmes/search', 'ProgrammeController@searchList')->name('programmes.searchList');

  Route::resource('pta', 'PtaController');
  Route::resource('ministeres', 'MinistereController');
  Route::resource('typestructures', 'TypestructureController');
  Route::resource('structures', 'StructureController');
  Route::resource('programmes', 'ProgrammeController');
  Route::resource('objectifs', 'ObjectifController');
  Route::resource('actions', 'ActionController');
  Route::resource('activites', 'ActiviteController');
  Route::resource('taches', 'TacheController');
  Route::resource('evaluations', 'EvaluationController');
  Route::resource('rapports', 'RapportController');

  Route::get('importexcelpta', 'ImportExcelPtaController@importExport')->name('pta.importexcel');

  Route::get('downloadExcel/{type}', 'ImportExcelPtaController@downloadExcel');

  Route::post('importExcel', 'ImportExcelPtaController@importExcel');

});

/*Route::get('/pta/{ptaId}', function () {
    return view('pta.show');
})->name('pta.show');*/
