var JAutocompletions = {

    getDepartementList: function () {
        //
    },

    getPtaCodeList: function () {
        $('#txtPta').typeahead({
            source: function (query, result) {
                $.ajax({
                    url: "/pta/autocomplete",
                    data: 'query=' + query,
                    dataType: "json",
                    type: "POST",
                    success: function (data) {
                        console.log(data);
                        result($.map(data, function (item) {
                            return item;
                        }));
                    }
                });
            }
        });
    }

}