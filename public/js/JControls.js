/**
 * Created by Admin on 21/08/2017.
 */
var JControls = {
    handleSelect: function (placeholder) {
        $.fn.select2.defaults.set("theme", "bootstrap");

        $(".select2, .select2-multiple").select2({
            language: "fr",
            placeholder: placeholder,
            width: null,
            allowClear: true
        });
    }
};

JControls.handleSelect('Veuillez sélectionner');
