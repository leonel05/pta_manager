-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Mer 07 Février 2018 à 15:14
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `pta_manager`
--

-- --------------------------------------------------------

--
-- Structure de la table `action`
--

CREATE TABLE `action` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_obj` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbrev` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `montant` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `nb_mois` int(10) UNSIGNED DEFAULT NULL,
  `associees` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poids` double(6,2) DEFAULT NULL,
  `operations` text COLLATE utf8mb4_unicode_ci,
  `niveau` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mnt_engage` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `taux_engage` double(5,3) NOT NULL DEFAULT '0.000',
  `mnt_ordonance` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `taux_ordonance` double(5,3) NOT NULL DEFAULT '0.000',
  `observations` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `action`
--

INSERT INTO `action` (`id`, `code`, `code_obj`, `abbrev`, `libelle`, `montant`, `nb_mois`, `associees`, `poids`, `operations`, `niveau`, `mnt_engage`, `taux_engage`, `mnt_ordonance`, `taux_ordonance`, `observations`, `created_at`, `updated_at`) VALUES
(1, 'PTA-2013-MCMEJF-PROG1-O1-A1', 'PTA-2013-MCMEJF-PROG1-O1', 'A1', 'Formation des SFD au respect de la réglementation', 22300, 4, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:30', '2017-10-16 15:46:30'),
(2, 'PTA-2013-MCMEJF-PROG1-O2-A1', 'PTA-2013-MCMEJF-PROG1-O2', 'A1', 'Renforcement des capacités techniques des SFD', 976106, 12, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(3, 'PTA-2013-MCMEJF-PROG1-O2-A2', 'PTA-2013-MCMEJF-PROG1-O2', 'A2', 'Renforcement des capacités du réseau MCPP', 150000, 12, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(4, 'PTA-2013-MCMEJF-PROG1-O2-A3', 'PTA-2013-MCMEJF-PROG1-O2', 'A3', 'Appui au recouvrement des créances impayées par les clients des SFD ', 14000, 12, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(5, 'PTA-2013-MCMEJF-PROG1-O2-A4', 'PTA-2013-MCMEJF-PROG1-O2', 'A4', 'Appui au recouvrement des créances impayées par les clients des SFD ', 12500, 12, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(6, 'PTA-2013-MCMEJF-PROG1-O3-A1', 'PTA-2013-MCMEJF-PROG1-O3', 'A1', 'Opérationnalisation du CNM', 41000, 12, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(7, 'PTA-2013-MCMEJF-PROG1-O3-A2', 'PTA-2013-MCMEJF-PROG1-O3', 'A2', 'Opérationnalisation des structures en charge de la micro finance ', 1875984, 12, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(8, 'PTA-2013-MCMEJF-PROG1-O3-A3', 'PTA-2013-MCMEJF-PROG1-O3', 'A3', 'Achat d\'équipement au profit des SFD partenaires du FNM ', 140000, 12, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(9, 'PTA-MFASSNHPTA-2015-PROG1-O1-A1', 'PTA-MFASSNHPTA-2015-PROG1-O1', 'A1', 'Action 1.1.1.1 : Augmenter les capacités d’intervention des structures déconcentrées (CPS, SS, DD, ect..) ', 0, NULL, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:41', '2017-12-19 18:50:41'),
(10, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'PTA-MFASSNHPTA-2015-PROG3-O2', 'A1', 'Action 3.2.1.2 : Accroitre l’efficience de la gestion du Ministère', 0, NULL, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(11, 'PTA-MFASSNHPTA-2014-PROG1-O1-A1', 'PTA-MFASSNHPTA-2014-PROG1-O1', 'A1', 'Action 1.1.1.1 : Augmenter les capacités d’intervention des structures déconcentrées (CPS, SS, DD, ect..) ', 0, NULL, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-20 16:54:40', '2017-12-20 16:54:40'),
(12, 'PTA-MFASSNHPTA-2015-PROG1-O1-A2', 'PTA-MFASSNHPTA-2015-PROG1-O1', 'A2', 'mon action', 5000, 3, NULL, 5.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2018-01-31 09:59:19', '2018-01-31 09:59:19');

-- --------------------------------------------------------

--
-- Structure de la table `activite`
--

CREATE TABLE `activite` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_action` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbrev` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `montant` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `nb_mois` int(10) UNSIGNED DEFAULT NULL,
  `associees` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poids` double(6,2) DEFAULT NULL,
  `operations` text COLLATE utf8mb4_unicode_ci,
  `niveau` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mnt_engage` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `taux_engage` double(5,3) NOT NULL DEFAULT '0.000',
  `mnt_ordonance` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `taux_ordonance` double(5,3) NOT NULL DEFAULT '0.000',
  `observations` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `activite`
--

INSERT INTO `activite` (`id`, `code`, `code_action`, `abbrev`, `libelle`, `montant`, `nb_mois`, `associees`, `poids`, `operations`, `niveau`, `mnt_engage`, `taux_engage`, `mnt_ordonance`, `taux_ordonance`, `observations`, `created_at`, `updated_at`) VALUES
(1, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT1', 'PTA-2013-MCMEJF-PROG1-O1-A1', 'ACT1', 'Atelier de sensibilisation des  Structures Financières opérant dans l\'informel sur la nécessité et l\'utilité de la formalisation (Deux regroupements dont un au Nord et un au Sud)', 7000, 4, NULL, 31.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:30', '2017-10-16 15:46:30'),
(2, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT2', 'PTA-2013-MCMEJF-PROG1-O1-A1', 'ACT2', 'Conception et réalisation d\'un guide des usagers de la micro finance', 5000, 3, NULL, 22.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(3, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT3', 'PTA-2013-MCMEJF-PROG1-O1-A1', 'ACT3', 'Sensibilisation et communication pour la promotion des meilleurs comportements des usagers (par le biais des médias)', 7000, 4, NULL, 31.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(4, 'PTA-MFASSNHPTA-2015-PROG1-O1-A1-ACT1', 'PTA-MFASSNHPTA-2015-PROG1-O1-A1', 'ACT1', 'Activité 1.1.1.1.1.1 Actualiser le plan de recrutement du ministère', 2, NULL, 'Toutes structures du ministère, MEF, MTFP', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:41', '2017-12-19 18:50:41'),
(5, 'PTA-MFASSNHPTA-2015-PROG1-O1-A1-ACT2', 'PTA-MFASSNHPTA-2015-PROG1-O1-A1', 'ACT2', 'Activité 1.1.1.1.2 Faire le plaidoyer pour la deuxième tranche du recrutement hors quota', 0, NULL, 'DRFM, Ministre', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(6, 'PTA-MFASSNHPTA-2015-PROG1-O1-A1-ACT3', 'PTA-MFASSNHPTA-2015-PROG1-O1-A1', 'ACT3', 'Activité 1.1.1.1.1.3 Introduire une communication en conseils des ministres ', 0, NULL, 'SGM, Cabinet', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(7, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1-ACT1', 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'ACT1', 'Activité 3.2.1.2.1: Organiser l\'évaluation des performances des agents chaque trimestre ', 0, NULL, 'Toutes les structures du ministère', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(8, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1-ACT2', 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'ACT2', 'Activité 3.2.1.2.2 Calculer, monter le dossier financier et faire payer les primes spécifiques au titre de 2014', 0, NULL, 'DRFM', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(9, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1-ACT3', 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'ACT3', 'Activité 3.2.1.2.3 Calculer, monter le dossier financier et faire payer les primes de rendement au titre de 2014 ', 0, NULL, 'DRFM', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(10, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1-ACT4', 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'ACT4', 'Activité 3.2.1.2.4 Organiser le recensement physique du personnel chaque année ', 7, NULL, 'Toutes les structures du ministère', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(11, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1-ACT5', 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'ACT5', 'Activité 3.2.1.2.5 Etablir les états nominatifs  du personnel au 01/01/2014 ', 3, NULL, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(12, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1-ACT6', 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'ACT6', 'Activité 3.2.1.2.6 Organiser les travaux de promotion des APE au titre de 2013 ', 2, NULL, 'Syndicats, MTFP', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(13, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1-ACT7', 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'ACT7', 'Activité 3.2.1.2.7 Organiser les travaux de titularisation au titre de 2013', 2, NULL, 'Syndicats, MTFPRAI-DS', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(14, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1-ACT8', 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'ACT8', 'Activité 3.2.1.2.8: Organiser les travaux d\'établissement des listes annuelles d\'aptitude', 1, NULL, 'Syndicats, MTFPRAI-DS', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(15, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1-ACT9', 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'ACT9', 'Activité 3.2.1.2.9: Organiser trois (03) sessions ordinaires du Conseil chargé de la promotion du dialogue social ', 20, NULL, 'Membres de cabinet, Directions centrales et syndicats', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(16, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1-ACT10', 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'ACT10', 'Activité 3.2.1.2.10: Acquérir des fournitures de bureau et consommables informatiques', 2, NULL, 'DRFM', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(17, 'PTA-MFASSNHPTA-2015-PROG3-O2-A1-ACT11', 'PTA-MFASSNHPTA-2015-PROG3-O2-A1', 'ACT11', 'Activité 3.2.1.2.11: Acquérir du carburant', 5, NULL, 'DRFM', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(18, 'PTA-MFASSNHPTA-2014-PROG1-O1-A1-ACT1', 'PTA-MFASSNHPTA-2014-PROG1-O1-A1', 'ACT1', 'Activité 1.1.1.1.1.1 Actualiser le plan de recrutement du ministère', 2, NULL, 'Toutes structures du ministère, MEF, MTFP', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-20 16:54:40', '2017-12-20 16:54:40'),
(19, 'PTA-MFASSNHPTA-2014-PROG1-O1-A1-ACT2', 'PTA-MFASSNHPTA-2014-PROG1-O1-A1', 'ACT2', 'Activité 1.1.1.1.2 Faire le plaidoyer pour la deuxième tranche du recrutement hors quota', 0, NULL, 'DRFM, Ministre', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-20 16:54:40', '2017-12-20 16:54:40'),
(20, 'PTA-MFASSNHPTA-2014-PROG1-O1-A1-ACT3', 'PTA-MFASSNHPTA-2014-PROG1-O1-A1', 'ACT3', 'Activité 1.1.1.1.1.3 Introduire une communication en conseils des ministres ', 0, NULL, 'SGM, Cabinet', NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-20 16:54:40', '2017-12-20 16:54:40'),
(21, 'PTA-MFASSNHPTA-2015-PROG1-O1-A2-ACT1', 'PTA-MFASSNHPTA-2015-PROG1-O1-A2', 'ACT1', 'activité', 1000, 3, NULL, 2.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2018-01-31 10:41:28', '2018-01-31 10:41:28');

-- --------------------------------------------------------

--
-- Structure de la table `evaluation`
--

CREATE TABLE `evaluation` (
  `id` int(10) UNSIGNED NOT NULL,
  `titre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `periode` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `date_creation` date NOT NULL,
  `code_pta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `evaluation`
--

INSERT INTO `evaluation` (`id`, `titre`, `type`, `periode`, `date_creation`, `code_pta`, `created_at`, `updated_at`) VALUES
(1, 'Tableau d\'évaluation au 31 décembre 2013', 'Trimestriel', 4, '2017-10-16', 'PTA-2013-MCMEJF', '2017-10-16 15:47:21', '2017-10-16 15:47:21');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_08_27_010230_create_ptas_table', 1),
(4, '2017_08_29_191054_create_ministeres_table', 1),
(5, '2017_08_29_191427_create_structures_table', 1),
(6, '2017_08_31_112405_create_typestructures_table', 1),
(7, '2017_09_26_231950_create_programmes_table', 2),
(36, '2017_09_28_210439_create_objectifs_table', 3),
(37, '2017_10_08_221721_create_actions_table', 3),
(38, '2017_10_08_222057_create_activites_table', 3),
(39, '2017_10_08_222303_create_taches_table', 3),
(40, '2017_10_11_183817_create_evaluations_table', 3),
(42, '2017_11_16_061737_create_rapports_table', 4);

-- --------------------------------------------------------

--
-- Structure de la table `ministere`
--

CREATE TABLE `ministere` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `ministere`
--

INSERT INTO `ministere` (`id`, `code`, `libelle`, `created_at`, `updated_at`) VALUES
(1, 'MAEP', 'Ministère de l\'Agriculture de l\'Elevage et de la Pêche', '2017-09-01 19:23:28', '2017-09-01 19:23:28'),
(2, 'MJSL', 'Ministère de la Jeunesse, des Sports et Loisirs', '2017-09-01 19:26:40', '2017-09-01 19:26:40'),
(3, 'MFASSNHPTA', 'MINISTERE DE LA FAMILLE, DES AFFAIRES SOCIALES, DE LA SOLIDARITE NATIONALE, DES HANDICAPES ET DES PERSONNES DE TROISIEME AGE', '2017-12-23 22:02:18', '2017-12-23 22:02:18');

-- --------------------------------------------------------

--
-- Structure de la table `objectif`
--

CREATE TABLE `objectif` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_prog` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbrev` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `resultat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `montant` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `nb_mois` int(10) UNSIGNED DEFAULT NULL,
  `responsable` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `associees` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poids` double(6,2) DEFAULT NULL,
  `operations` text COLLATE utf8mb4_unicode_ci,
  `niveau` int(10) UNSIGNED DEFAULT '0',
  `mnt_engage` int(10) UNSIGNED DEFAULT '0',
  `taux_engage` double(5,3) NOT NULL DEFAULT '0.000',
  `mnt_ordonance` int(10) UNSIGNED DEFAULT '0',
  `taux_ordonance` double(5,3) NOT NULL DEFAULT '0.000',
  `observations` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `objectif`
--

INSERT INTO `objectif` (`id`, `code`, `code_prog`, `abbrev`, `libelle`, `resultat`, `montant`, `nb_mois`, `responsable`, `associees`, `poids`, `operations`, `niveau`, `mnt_engage`, `taux_engage`, `mnt_ordonance`, `taux_ordonance`, `observations`, `created_at`, `updated_at`) VALUES
(1, 'PTA-2013-MCMEJF-PROG1-O1', 'PTA-2013-MCMEJF-PROG1', 'O1', 'Appuyer  l\'institutionnalisation des structures financières ', 'Les structures financières opérant dans l\'informel sont institutionnalisées', 200000, NULL, NULL, NULL, NULL, 'ctctxyct', 10, 5000, 2.500, 4000, 2.000, 'futcbubglyuchgvhiv j ucf jhvbmiy', '2017-10-16 15:46:30', '2017-11-16 17:31:11'),
(2, 'PTA-2013-MCMEJF-PROG1-O2', 'PTA-2013-MCMEJF-PROG1', 'O2', 'Renforcer les capacités techniques et financières des SFD', 'Les capacités techniques et financières des SFD sont renforcées', 150000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:31', '2017-11-15 17:14:36'),
(3, 'PTA-2013-MCMEJF-PROG1-O3', 'PTA-2013-MCMEJF-PROG1', 'O3', 'Renforcer les capacités techniques et financières des SFD', 'Les capacités des structures de promotion de la micro finance sont opérationnelles et renforcées', 160000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:31', '2017-11-15 17:14:36'),
(4, 'PTA-2013-MCMEJF-PROG1-O4', 'PTA-2013-MCMEJF-PROG1', 'O4', 'Intégrer la microfinance  au secteur financier', 'Les SFD sont mieux intégrés au système financier ', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:31', '2017-11-15 17:14:36'),
(5, 'PTA-2013-MCMEJF-PROG1-O5', 'PTA-2013-MCMEJF-PROG1', 'O5', 'Intégrer la microfinance  au secteur financier', 'Les banques sont appuyées pour la promotion de nouveaux produits financiers à l\'endroit des SFD', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:31', '2017-11-15 17:14:36'),
(6, 'PTA-2013-MCMEJF-PROG1-O6', 'PTA-2013-MCMEJF-PROG1', 'O6', 'Promouvoir la finance inclusive', 'Les activités économiques en milieu rural sont financées', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:31', '2017-11-15 17:14:36'),
(7, 'PTA-2013-MCMEJF-PROG1-O7', 'PTA-2013-MCMEJF-PROG1', 'O7', 'Promouvoir la finance inclusive', 'Des mesures incitatives sont initiées pour favoriser l’extension ou l’installation des SFD en milieu rural ', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:31', '2017-11-15 17:14:36'),
(8, 'PTA-2013-MCMEJF-PROG1-O8', 'PTA-2013-MCMEJF-PROG1', 'O8', 'Promouvoir la finance inclusive', 'Des lignes de crédit sont mises en place pour le refinancement des SFD', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:32', '2017-11-15 17:14:36'),
(9, 'PTA-2013-MCMEJF-PROG2-O1', 'PTA-2013-MCMEJF-PROG2', 'O1', ' Généraliser  l’approche Développement Conduit par les Communautés', 'L\'approche Développement Conduit par les Communautés (DCC) est appropriée par les acteurs à la base', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:32', '2017-11-15 17:14:36'),
(10, 'PTA-2013-MCMEJF-PROG2-O2', 'PTA-2013-MCMEJF-PROG2', 'O2', ' Accroitre  la capacité organisationnelle et technique des couches les plus vulnérables', 'Les créneaux porteurs à la base sont promus', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:32', '2017-11-15 17:14:36'),
(11, 'PTA-2013-MCMEJF-PROG3-O1', 'PTA-2013-MCMEJF-PROG3', 'O1', 'consolider les acquis et favoriser la création de nouveau emploi', 'Les initiatives en faveur de l\'emploi salarié sont mises en œuvre', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:32', '2017-11-15 17:14:36'),
(12, 'PTA-2013-MCMEJF-PROG3-O2', 'PTA-2013-MCMEJF-PROG3', 'O2', 'consolider les acquis et favoriser la création de nouveau emploi', 'Les initiatives en faveur de l\'auto emploi sont assurées', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:32', '2017-11-15 17:14:36'),
(13, 'PTA-2013-MCMEJF-PROG3-O3', 'PTA-2013-MCMEJF-PROG3', 'O3', 'consolider les acquis et favoriser la création de nouveau emploi', 'Les structures en charge de l\'emploi sont fonctionnelles ', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:32', '2017-11-15 17:14:36'),
(14, 'PTA-2013-MCMEJF-PROG4-O1', 'PTA-2013-MCMEJF-PROG4', 'O1', ' Administration et Gestion des Services', 'Les documents de planification du ministère sont mieux élaborés, mis en œuvre et évalués', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:32', '2017-11-15 17:14:36'),
(15, 'PTA-2013-MCMEJF-PROG4-O2', 'PTA-2013-MCMEJF-PROG4', 'O2', ' Administration et Gestion des Services', 'Les Ressources humaines sont performantes', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:32', '2017-11-15 17:14:36'),
(16, 'PTA-2013-MCMEJF-PROG4-O3', 'PTA-2013-MCMEJF-PROG4', 'O3', ' Administration et Gestion des Services', 'Les ressources financières et matérielles sont mieux mobilisées', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:32', '2017-11-15 17:14:36'),
(17, 'PTA-2013-MCMEJF-PROG4-O4', 'PTA-2013-MCMEJF-PROG4', 'O4', ' Administration et Gestion des Services', 'Les Ressources financières et matérielles allouées au Ministère sont mieux gérées', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.000, NULL, 0.000, NULL, '2017-10-16 15:46:32', '2017-11-15 17:14:36'),
(18, 'PTA-MFASSNHPTA-2015-PROG1-O1', 'PTA-MFASSNHPTA-2015-PROG1', 'O1', 'Objectif Spécifique 1.1 : Mettre à disposition du public des services sociaux de proximité ', 'Résultat 1.1.1 : L’ensemble des prestations du paquet minimum de prestations sociales est délivré dans toutes les communes et des prestations complémentaires sont offertes à la population.', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:41', '2017-12-19 18:50:41'),
(19, 'PTA-MFASSNHPTA-2015-PROG3-O1', 'PTA-MFASSNHPTA-2015-PROG3', 'O1', 'Objectif spécifique 3.1 : Internaliser la gestion axée sur les résultats', 'Résultat 3.1.1 : L’ensemble des actions du Ministère sont alignées sur les objectifs spécifiques du Plan Stratégique et sont  régulièrement évaluées et mesurées par rapport aux résultats attendus.', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(20, 'PTA-MFASSNHPTA-2015-PROG3-O2', 'PTA-MFASSNHPTA-2015-PROG3', 'O2', 'Objectif spécifique 3.2 : Augmenter les performances et la visibilité des actions du Ministère', 'Résultat 3.2.1 : Le Ministère utilise de façon efficiente des ressources accrues dédiées à la protection des groupes vulnérables.', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(21, 'PTA-MFASSNHPTA-2014-PROG1-O1', 'PTA-MFASSNHPTA-2014-PROG1', 'O1', 'Objectif Spécifique 1.1 : Mettre à disposition du public des services sociaux de proximité ', 'Résultat 1.1.1 : L’ensemble des prestations du paquet minimum de prestations sociales est délivré dans toutes les communes et des prestations complémentaires sont offertes à la population.', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-20 16:54:40', '2017-12-20 16:54:40'),
(22, 'PTA-MFASSNHPTA-2014-PROG3-O1', 'PTA-MFASSNHPTA-2014-PROG3', 'O1', 'Objectif spécifique 3.1 : Internaliser la gestion axée sur les résultats', 'Résultat 3.1.1 : L’ensemble des actions du Ministère sont alignées sur les objectifs spécifiques du Plan Stratégique et sont  régulièrement évaluées et mesurées par rapport aux résultats attendus.', 0, NULL, NULL, NULL, NULL, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-12-20 16:54:40', '2017-12-20 16:54:40'),
(23, 'PTA-MFASSNHPTA-2015-PROG1-O2', 'PTA-MFASSNHPTA-2015-PROG1', 'O2', 'objectif', 'resultat', 25000, 11, NULL, NULL, 10.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2018-01-31 09:47:08', '2018-01-31 09:47:08');

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `programme`
--

CREATE TABLE `programme` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `objectif` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_pta` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `programme`
--

INSERT INTO `programme` (`id`, `code`, `libelle`, `objectif`, `code_pta`, `created_at`, `updated_at`) VALUES
(1, 'PTA-2013-MCMEJF-PROG1', 'DEVELOPPEMENT DE LA MICROFINANCE', 'Promouvoir une micro finance professionnelle, viable et intégrée au secteur financier', 'PTA-2013-MCMEJF', '2017-10-16 15:46:30', '2017-10-16 15:46:30'),
(2, 'PTA-2013-MCMEJF-PROG2', 'PROMOTION DES INITIATIVES LOCALES', 'Contribuer efficacement à la création  de la richesse', 'PTA-2013-MCMEJF', '2017-10-16 15:46:32', '2017-10-16 15:46:32'),
(3, 'PTA-2013-MCMEJF-PROG3', 'Promotion de l\'Emploi', 'Consolider les acquis et favoriser la création de nouveau emploi', 'PTA-2013-MCMEJF', '2017-10-16 15:46:32', '2017-10-16 15:46:32'),
(4, 'PTA-2013-MCMEJF-PROG4', 'Administration et Gestion des Services', 'Améliorer la gestion des ressources du Ministère et à assurer la mise en oeuvre des stratégies du Ministère', 'PTA-2013-MCMEJF', '2017-10-16 15:46:32', '2017-10-16 15:46:32'),
(5, 'PTA-MFASSNHPTA-2015-PROG1', 'PROGRAMME N° 1 RENFORCEMENT DE LA PROTECTION SOCIALE ET PROMOTION DE LA SOLIDARITE NATIONALE', 'Objectif Général 1 : Améliorer le bien être de la population à travers le renforcement de la protection sociale et la promotion de la Solidarité Nationale ', 'PTA-MFASSNHPTA-2015', '2017-12-19 18:50:41', '2017-12-19 18:50:41'),
(6, 'PTA-MFASSNHPTA-2015-PROG3', 'PROGRAMME N° 3 RENFORCEMENT DE LA GOUVERNANCE ET DE L\'EFFICACITE DU MINISTERE', 'Objectif Général 3: Améliorer la gouvernance et la performance du Ministère ', 'PTA-MFASSNHPTA-2015', '2017-12-19 18:50:42', '2017-12-19 18:50:42'),
(7, 'PTA-MFASSNHPTA-2014-PROG1', 'PROGRAMME N° 1 RENFORCEMENT DE LA PROTECTION SOCIALE ET PROMOTION DE LA SOLIDARITE NATIONALE', 'Objectif Général 1 : Améliorer le bien être de la population à travers le renforcement de la protection sociale et la promotion de la Solidarité Nationale ', 'PTA-MFASSNHPTA-2014', '2017-12-20 16:54:40', '2017-12-20 16:54:40'),
(8, 'PTA-MFASSNHPTA-2014-PROG3', 'PROGRAMME N° 3 RENFORCEMENT DE LA GOUVERNANCE ET DE L\'EFFICACITE DU MINISTERE', 'Objectif Général 3: Améliorer la gouvernance et la performance du Ministère ', 'PTA-MFASSNHPTA-2014', '2017-12-20 16:54:40', '2017-12-20 16:54:40');

-- --------------------------------------------------------

--
-- Structure de la table `pta`
--

CREATE TABLE `pta` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `obj_global` text COLLATE utf8mb4_unicode_ci,
  `code_ministere` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_structure` varchar(10) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `exercice` varchar(4) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `pta`
--

INSERT INTO `pta` (`id`, `code`, `obj_global`, `code_ministere`, `code_structure`, `exercice`, `created_at`, `updated_at`) VALUES
(1, 'PTA-2013-MCMEJF', 'Développement', 'MCMEJF', 'DRH', '2013', '2017-10-16 15:46:30', '2017-10-16 15:46:30'),
(2, 'PTA-MFASSNHPTA-2015', NULL, 'MFASSNHPTA', NULL, '2015', '2017-12-19 18:50:41', '2017-12-19 18:50:41'),
(3, 'PTA-MFASSNHPTA-2014', NULL, 'MFASSNHPTA', NULL, '2014', '2017-12-20 16:54:40', '2017-12-20 16:54:40');

-- --------------------------------------------------------

--
-- Structure de la table `rapport`
--

CREATE TABLE `rapport` (
  `id` int(10) UNSIGNED NOT NULL,
  `resume` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `introduction` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `objectifs` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `resultats` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `resources` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `activites_prevues` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `activites_non_prevues` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `bilan_financier` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `difficultes_recommandations` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `conclusion` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `evaluation` int(10) UNSIGNED NOT NULL,
  `auteur` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `rapport`
--

INSERT INTO `rapport` (`id`, `resume`, `introduction`, `objectifs`, `resultats`, `resources`, `activites_prevues`, `activites_non_prevues`, `bilan_financier`, `difficultes_recommandations`, `conclusion`, `evaluation`, `auteur`, `created_at`, `updated_at`) VALUES
(1, 'Resumé2.0', 'Introduction', 'Objectifs', 'Résultats', 'Ressources allouées', 'Activités prévues', 'Activités non prévues', 'Bilan financier', 'Difficultés rencontrées et recommandations', 'Conclusion', 1, NULL, '2017-12-23 21:34:49', '2017-12-25 20:57:07');

-- --------------------------------------------------------

--
-- Structure de la table `structure`
--

CREATE TABLE `structure` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_typestructure` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_ministere` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `structure`
--

INSERT INTO `structure` (`id`, `code`, `libelle`, `code_typestructure`, `code_ministere`, `created_at`, `updated_at`) VALUES
(1, 'DRH', 'Direction des Ressources Humaines', 'DC', 'MAEP', '2017-09-01 20:47:52', '2017-09-01 20:47:52');

-- --------------------------------------------------------

--
-- Structure de la table `tache`
--

CREATE TABLE `tache` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code_act` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abbrev` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelle` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `montant` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `nb_mois` int(10) UNSIGNED DEFAULT NULL,
  `associees` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `poids` double(6,2) DEFAULT NULL,
  `operations` text COLLATE utf8mb4_unicode_ci,
  `niveau` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `mnt_engage` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `taux_engage` double(5,3) NOT NULL DEFAULT '0.000',
  `mnt_ordonance` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `taux_ordonance` double(5,3) NOT NULL DEFAULT '0.000',
  `observations` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `tache`
--

INSERT INTO `tache` (`id`, `code`, `code_act`, `abbrev`, `libelle`, `montant`, `nb_mois`, `associees`, `poids`, `operations`, `niveau`, `mnt_engage`, `taux_engage`, `mnt_ordonance`, `taux_ordonance`, `observations`, `created_at`, `updated_at`) VALUES
(1, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT1-T1', 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT1', 'T1', 'Elaboration des TDR', 0, 3, NULL, 10.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:30', '2017-10-16 15:46:30'),
(2, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT1-T2', 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT1', 'T2', 'Réalisation des séances  sensibilisation en deux regroupements dont un au Nord et un au Sud', 7000, 3, NULL, 90.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:30', '2017-10-16 15:46:30'),
(3, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT2-T1', 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT2', 'T1', 'Elaboration des TDR', 0, 5, NULL, 10.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(4, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT2-T2', 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT2', 'T2', 'Recrutement des prestataires', 0, 5, NULL, 10.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(5, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT2-T3', 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT2', 'T3', 'Edition et reception de 2000 exemplaires du guide', 5000, 5, NULL, 30.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(6, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT2-T4', 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT2', 'T4', 'Diffusion du guide des usagers', 0, 6, NULL, 50.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(7, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT3-T1', 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT3', 'T1', 'Elaboration des TDR', 0, 6, NULL, 10.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(8, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT3-T2', 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT3', 'T2', 'Conception des messages, spots publicitaires, sketchs et émissions', 2000, 9, NULL, 25.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(9, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT3-T3', 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT3', 'T3', 'Sélection d’agences de communication et signature du contrat', 0, 10, NULL, 15.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(10, 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT3-T4', 'PTA-2013-MCMEJF-PROG1-O1-A1-ACT3', 'T4', 'Diffusion sur les médias (radios, télévisions)', 5000, 11, NULL, 50.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2017-10-16 15:46:31', '2017-10-16 15:46:31'),
(11, 'PTA-MFASSNHPTA-2015-PROG1-O1-A2-ACT1-T1', 'PTA-MFASSNHPTA-2015-PROG1-O1-A2-ACT1', 'T1', 'tache unique', 1000, 5, NULL, 100.00, NULL, 0, 0, 0.000, 0, 0.000, NULL, '2018-01-31 11:05:29', '2018-01-31 11:05:29');

-- --------------------------------------------------------

--
-- Structure de la table `typestructure`
--

CREATE TABLE `typestructure` (
  `id` int(10) UNSIGNED NOT NULL,
  `code` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `libelle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Contenu de la table `typestructure`
--

INSERT INTO `typestructure` (`id`, `code`, `libelle`, `created_at`, `updated_at`) VALUES
(1, 'DC', 'Direction du Cabinet', '2017-09-01 19:39:01', '2017-09-01 19:39:01');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `action`
--
ALTER TABLE `action`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `action_code_unique` (`code`);

--
-- Index pour la table `activite`
--
ALTER TABLE `activite`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `activite_code_unique` (`code`);

--
-- Index pour la table `evaluation`
--
ALTER TABLE `evaluation`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `ministere`
--
ALTER TABLE `ministere`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `objectif`
--
ALTER TABLE `objectif`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `objectif_code_unique` (`code`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `programme`
--
ALTER TABLE `programme`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Index pour la table `pta`
--
ALTER TABLE `pta`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `code` (`code`);

--
-- Index pour la table `rapport`
--
ALTER TABLE `rapport`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `structure`
--
ALTER TABLE `structure`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `tache`
--
ALTER TABLE `tache`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tache_code_unique` (`code`);

--
-- Index pour la table `typestructure`
--
ALTER TABLE `typestructure`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `action`
--
ALTER TABLE `action`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `activite`
--
ALTER TABLE `activite`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `evaluation`
--
ALTER TABLE `evaluation`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT pour la table `ministere`
--
ALTER TABLE `ministere`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `objectif`
--
ALTER TABLE `objectif`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
--
-- AUTO_INCREMENT pour la table `programme`
--
ALTER TABLE `programme`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT pour la table `pta`
--
ALTER TABLE `pta`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `rapport`
--
ALTER TABLE `rapport`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `structure`
--
ALTER TABLE `structure`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `tache`
--
ALTER TABLE `tache`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `typestructure`
--
ALTER TABLE `typestructure`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
