<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Objectif extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'objectif';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['code','code_prog', 'abbrev', 'libelle', 'resultat', 'montant', 'nb_mois', 'responsable',
        'associees', 'poids', 'operations', 'niveau', 'mnt_engage', 'taux_engage', 'mnt_ordonance', 'taux_ordonance', 'observations'];

    /**
     * Attributes that aren't in database
     *
     * @var array
     */
    protected $appends = [];
}
