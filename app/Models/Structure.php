<?php

namespace App\Models;

use App\Models\Model;

class Structure extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'structure';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'libelle', 'code_typestructure', 'code_ministere'];

    /**
     * Attributes that aren't in database
     *
     * @var array
     */
    protected $appends = [];
}
