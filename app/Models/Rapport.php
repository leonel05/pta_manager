<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Rapport extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'rapport';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['resume', 'introduction', 'objectifs', 'resultats', 'resources', 'activites_prevues',
        'activites_non_prevues', 'bilan_financier', 'difficultes_recommandations', 'conclusion', 'evaluation',
        'auteur'];

    /**
     * Attributes that aren't in database
     *
     * @var array
     */
    protected $appends = [];
}
