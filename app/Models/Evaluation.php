<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Evaluation extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'evaluation';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['titre', 'type', 'periode', 'date_creation', 'code_pta'];

    /**
     * Attributes that aren't in database
     *
     * @var array
     */
    protected $appends = [];
}
