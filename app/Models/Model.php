<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    public function setLocalConnection(){
        $this->setConnection('mysql2');
    }

    public function setExternalConnection(){
        $this->setConnection('mysql1');
    }
}
