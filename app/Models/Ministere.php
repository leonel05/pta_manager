<?php

namespace App\Models;

use App\Models\Model;
class Ministere extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'ministere';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'libelle'];

    /**
     * Attributes that aren't in database
     *
     * @var array
     */
    protected $appends = [];

}
