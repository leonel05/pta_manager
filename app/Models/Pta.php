<?php

namespace App\Models;

use App\Models\Model;

class Pta extends Model
{
    //

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'pta';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['code', 'obj_global', 'code_ministere', 'code_structure', 'exercice'];

    /**
     * Attributes that aren't in database
     *
     * @var array
     */
    protected $appends = [];

}
