<?php

namespace App\Http\Controllers;

use App\Models\Ministere;
use App\Models\Structure;
use App\Models\Typestructure;
use Illuminate\Http\Request;

class StructureController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        //
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $structures = Structure::orderBy('code')->get();

        $data = compact('structures');
        return view('structures.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $structure = new Structure();
        $typestructures = Typestructure::orderBy('code')->get()->pluck('libelle', 'code');
        $ministeres = Ministere::orderBy('code')->get()->pluck('libelle', 'code');

        $data = compact('structure', 'typestructures', 'ministeres');

        return view('structures.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $structure = Structure::create($request->all());

        if ($structure){
            return redirect(route('structures.create'));
        }

        $data = compact('structure');

        return view('structures.form', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
