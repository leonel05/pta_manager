<?php

namespace App\Http\Controllers;

use App\Models\Objectif;
use App\Models\Programme;
use Illuminate\Http\Request;

class ObjectifController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $objectifs = Objectif::orderBy('code')->get();

        $data = compact('objectifs');
        return view('objectifs.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $objectif = new Objectif();
        $prog_codes = Programme::orderBy('code')->get()->pluck('code', 'code');
        $mois = array(
            '1' => 'Janvier',
            '2' => 'Février',
            '3' => 'Mars',
            '4' => 'Avril',
            '5' => 'Mai',
            '6' => 'Juin',
            '7' => 'Juillet',
            '8' => 'Août',
            '9' => 'Septembre',
            '10' => 'Octobre',
            '11' => 'Novembre',
            '12' => 'Décembre',
        );

        $data = compact('objectif', 'mois',  'prog_codes');

        return view('objectifs.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nbr_obj = Objectif::where('code_prog', '=', $request['code_prog'])->count();
        $request['code'] = $request['code_prog'].'-O'.($nbr_obj + 1);
        $request['abbrev'] = 'O'.($nbr_obj + 1);

        $debut = $request['debut'];
        $fin = $request['fin'];

        if($fin < $debut){
            $request['nb_mois'] = (((int)$fin + 12) - (int)$debut) + 1;
        }
        else{
            $request['nb_mois'] = ((int)$fin - (int)$debut) + 1;
        }

        $objectif = Objectif::create($request->all());

        if ($objectif){
            return redirect(route('objectifs.create'));
        }

        $data = compact('objectif');

        return view('objectifs.form', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
