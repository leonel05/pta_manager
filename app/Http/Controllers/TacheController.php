<?php

namespace App\Http\Controllers;

use App\Models\Activite;
use App\Models\Tache;
use Illuminate\Http\Request;

class TacheController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taches = Tache::orderBy('code')->get();

        $data = compact('taches');
        return view('taches.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tache = new Tache();
        $act_codes = Activite::orderBy('code')->get()->pluck('code', 'code');
        $mois = array(
            '1' => 'Janvier',
            '2' => 'Février',
            '3' => 'Mars',
            '4' => 'Avril',
            '5' => 'Mai',
            '6' => 'Juin',
            '7' => 'Juillet',
            '8' => 'Août',
            '9' => 'Septembre',
            '10' => 'Octobre',
            '11' => 'Novembre',
            '12' => 'Décembre',
        );

        $data = compact('tache', 'mois',  'act_codes');

        return view('taches.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nbr_taches = Tache::where('code_act', '=', $request['code_act'])->count();
        $request['code'] = $request['code_act'].'-T'.($nbr_taches + 1);
        $request['abbrev'] = 'T'.($nbr_taches + 1);

        $debut = $request['debut'];
        $fin = $request['fin'];

        if($fin < $debut){
            $request['nb_mois'] = (((int)$fin + 12) - (int)$debut) + 1;
        }
        else{
            $request['nb_mois'] = ((int)$fin - (int)$debut) + 1;
        }

        $tache = Tache::create($request->all());

        if ($tache){
            return redirect(route('taches.create'));
        }

        $data = compact('tache');

        return view('taches.form', $data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
