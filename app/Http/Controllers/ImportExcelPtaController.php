<?php
namespace App\Http\Controllers;


use Illuminate\Http\Request;

use Maatwebsite\Excel\Facades\Excel;
use PHPExcel_IOFactory;
use PHPExcel_Shared_Date;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;

use App\Models\Action;
use App\Models\Activite;
use App\Models\Ministere;
use App\Models\Objectif;
use App\Models\Programme;
use App\Models\Pta;
use App\Models\Structure;
use App\Models\Tache;



class ImportExcelPtaController extends Controller

{


	/**
     * Return View file
     *
     * @var array
     */

	public function importExport()

	{
		return view('pta.importexcel');

	}



	/**
     * Import file into database Code
     *
     * @var array
     */

	public function importExcel(Request $request)

	{

		try
		{

					$file = Input::file('import_file');
			        $rules = [
			            'import_file' => 'required'
			        ];
			        $messages = [
			            'import_file.required' => 'Veuillez sélectionner le fichier à importer'
			        ];

			        $validator = Validator::make($request->all(), $rules, $messages);
			        if ($validator->fails()) {
			            return Redirect::to('/importexcelpta')->withErrors($validator);
			        } else {

			            $autorizedExtensions = array("csv", "xls", "xlsx");
			            if ($file->isValid() && $file->getCLientSize() > 0) {
			                if (in_array($file->getClientOriginalExtension(), $autorizedExtensions)) {

			                	$destinationPath = public_path() . '/uploads'; // upload path
			                    $extension = $file->getClientOriginalExtension(); // getting image extension
			                    $fileName = 'pta' . date('Ymd') . '.' . $extension; // renaming image
			                    $file->move($destinationPath, $fileName);
			                    $filePath = $destinationPath . '/' . $fileName;



			                    // Create Reader
				                $objReader = PHPExcel_IOFactory::createReaderForFile($filePath);
				                $objReader->setReadDataOnly(true);
				                $objPHPExcel = $objReader->load($filePath);

				                // Processing Excel
				                $objWorksheet = $objPHPExcel->getActiveSheet(); // first sheet
				                $highestRow = $objWorksheet->getHighestRow(); // here '5'
				                $highestColumn = $objWorksheet->getHighestColumn(); // here 'E'

				                // Saving in database
				                $insert = [];
				                $errors = [];
				                $siErreur = false;
				                $saved = false;

				                /* Récupération des données du PTA */
				                $codeministere = trim($objPHPExcel->getActiveSheet()->getCell('A1')->getValue());
				                $annee = trim($objPHPExcel->getActiveSheet()->getCell('B3')->getValue());

				                $codepta="PTA-".$codeministere."-".$annee;

				                //Vérifier si le pta a déja été importé

				                $PtaSearch = PTA::where("code","=",$codepta)->get();

								if(!$PtaSearch->isEmpty())
								{
									$type='danger';

					                $message="Ce PTA existe déjà.";

							        $data = compact('pta', 'type', 'message');

							        return view('pta.importexcel', $data);
								}

				                $pta=new PTA;

				                $pta->code=$codepta;



				                $pta->code_ministere=$codeministere;

				                $pta->exercice=$annee;

				                //Save in PTA table
				                $pta->save();


				                $numprogramme=1;

				                $codeprogramme="";


								$codeobj="";
								$libelleobj="";
								$montant="";
								$resp="";
								$assoc="";
								$poid="";
								$numobj=1;
								$numaction=1;
								$numactivite=1;

				                /* On parcours le reste des lignes */
				                for ($row = 4; $row <= $highestRow; ++$row) {
				                    
				                    $cells = trim($objPHPExcel->getActiveSheet()->getCell('A' . $row)->getValue());


				                    if (strpos(strtoupper($cells), "PROGRAMME") !== false)
									{
										

										$codeprogramme=$codepta."-PROG".$numprogramme;
										$libelleprog=$cells;

										$numprogramme++;

									}

									if(preg_match("#OG[0-9]+#",$cells))
									{
										$objectifGeneProg=$objPHPExcel->getActiveSheet()->getCell('B' . $row)->getValue();


										$programme=new PROGRAMME;
										$programme->code=$codeprogramme;

										$programme->libelle=$libelleprog;

										$programme->code_pta=$codepta;

										$programme->objectif=$objectifGeneProg;

										$programme->save();


				                		$numobj=1;


									}

									if(preg_match("#OS[0-9\.]+#",$cells))
									{
										

										$codeobj=$codeprogramme."-O".$numobj;
										$libelleobj=$objPHPExcel->getActiveSheet()->getCell('B' . $row)->getValue();
										$montant=$objPHPExcel->getActiveSheet()->getCell('F' . $row)->getValue();
										$resp=$objPHPExcel->getActiveSheet()->getCell('G' . $row)->getValue();
										$assoc=$objPHPExcel->getActiveSheet()->getCell('H' . $row)->getValue();
										$poids=$objPHPExcel->getActiveSheet()->getCell('V' . $row)->getValue();

										$numaction=1;

									}

									if(preg_match("#R[0-9\.]+#",$cells))
									{

										$objectif=new Objectif;
										$objectif->code=$codeobj;

										$objectif->libelle=$libelleobj;
										$objectif->abbrev="O".$numobj;
										$objectif->code_prog=$codeprogramme;
										$objectif->libelle=$libelleobj;

										$resultatObj=$cells;

										if(($montant=="") or(is_null($montant)))
											$objectif->montant=0;
										else
											$objectif->montant=$montant;

										$objectif->responsable=$resp;
										$objectif->associees=$assoc;
										$objectif->poids=$poids;

										$objectif->resultat=$objPHPExcel->getActiveSheet()->getCell('B' . $row)->getValue();

										

										$objectif->save();


										$numobj++;


									}


									if(preg_match("#A[0-9\.]+#",$cells))
									{

										$action=new Action;
										$codeaction=$codeobj."-A".$numaction;
										$action->code=$codeaction;

										$libelleAction=$objPHPExcel->getActiveSheet()->getCell('B' . $row)->getValue();

										$montantAction=$objPHPExcel->getActiveSheet()->getCell('F' . $row)->getValue();

										$assoc=$objPHPExcel->getActiveSheet()->getCell('H' . $row)->getValue();
										$poids=$objPHPExcel->getActiveSheet()->getCell('V' . $row)->getValue();

										
										if(($montantAction=="") or(is_null($montantAction)))
											$action->montant=0;
										else
											$action->montant=$montantAction;

										

										$action->code_obj=$codeobj;
										$action->abbrev="A".$numaction;
										$action->libelle=$libelleAction;

										$action->associees=$assoc;
										//$action->poids=$poids;
										$action->save();

										$numaction++;

										$numactivite=1;

									}


									if(preg_match("#a[0-9\.]+#",$cells))
									{

										$activite=new activite;
										$activite->code=$codeaction."-ACT".$numactivite;

										$libelleactivite=$objPHPExcel->getActiveSheet()->getCell('B' . $row)->getValue();

										$montantactivite=$objPHPExcel->getActiveSheet()->getCell('F' . $row)->getCalculatedValue();

										$assoc=$objPHPExcel->getActiveSheet()->getCell('H' . $row)->getValue();
										$poids=$objPHPExcel->getActiveSheet()->getCell('V' . $row)->getValue();

										
										if(($montantactivite=="") or(is_null($montantactivite)))
											$activite->montant=0;
										else
											$activite->montant=$montantactivite;

										$activite->code_action=$codeaction;
										$activite->abbrev="ACT".$numactivite;
										$activite->libelle=$libelleactivite;

										$activite->associees=$assoc;
										
										$activite->save();

										$numactivite++;

									}


				                }

				                $type='success';

				                $message="Le fichier a été importé avec succès.";

						        $data = compact('pta', 'type', 'message');

						        return view('pta.importexcel', $data);

			                }
			            }

			        }
			    } catch(\Illuminate\Database\QueryException $ex){
            
			                    $type = 'danger';
						        $message = "Erreur lors de l'importation du PTA";

						        $data = compact('pta', 'type', 'message');

						        return view('pta.importexcel', $data);

        }catch(\Exception $e){
             
			                    $type = 'danger';
						        $message = "Erreur lors de l'importation du PTA";

						        $data = compact('pta', 'type', 'message');

						        return view('pta.importexcel', $data);
        } 

	}


}