<?php

namespace App\Http\Controllers;

use App\Models\Action;
use App\Models\Activite;
use App\Models\Evaluation;
use App\Models\Objectif;
use App\Models\Programme;
use App\Models\Pta;
use App\Models\Tache;
use Carbon\Carbon;
use Illuminate\Http\Request;

class EvaluationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $evaluations = Evaluation::orderBy('date_creation')->get();

        $data = compact('evaluations');
        return view('evaluations.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $evaluation = new Evaluation();
        $pta_codes = Pta::orderBy('code')->get()->pluck('code', 'code');
        $types = array(
            'Mensuel' => 'Mensuel',
            'Trimestriel' => 'Trimestre',
            'Semestriel' => 'Semestre',
            'Annuel' => 'Annuel'
        );

        $data = compact('evaluation', 'types',  'pta_codes');

        return view('evaluations.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request['date_creation'] = Carbon::now();
        $evaluation = Evaluation::create($request->all());

        if ($evaluation){
            return redirect(route('evaluations.edit', $evaluation->id));
        }

        return redirect(route('evaluations.create'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $isView = true;
        $evaluation = Evaluation::where('id', '=', $id)->get()->first();

        $pta = Pta::where('code', '=',  $evaluation->code_pta)->get()->first();
        $annee = $pta->exercice;
        //$ministere = Ministere::where('code', '=', $pta->code_ministere)->get()->first();
        //$ministere = $ministere->libelle;
        $ministere = $pta->code_ministere;

        $programmes = Programme::where('code_pta', '=', $pta->code)->get();

        foreach ($programmes as $programme){
            $objectifs[$programme->code] = Objectif::where('code_prog', '=', $programme->code)->get();

            foreach ($objectifs[$programme->code] as $objectif){
                $actions[$objectif->code] = Action::where('code_obj', '=', $objectif->code)->get();

                foreach ($actions[$objectif->code] as $action){
                    $activites[$action->code] = Activite::where('code_action', '=', $action->code)->get();

                    foreach ($activites[$action->code] as $activite){
                        $taches[$activite->code] = Tache::where('code_act', '=', $activite->code)->get();
                    }
                }
            }
        }

        $data = compact('evaluation', 'isView', 'programmes', 'objectifs', 'actions', 'activites', 'taches', 'annee', 'ministere');
        return view('evaluations.edit', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $evaluation = Evaluation::where('id', '=', $id)->get()->first();

        $pta = Pta::where('code', '=',  $evaluation->code_pta)->get()->first();
        $annee = $pta->exercice;
        //$ministere = Ministere::where('code', '=', $pta->code_ministere)->get()->first();
        //$ministere = $ministere->libelle;
        $ministere = $pta->code_ministere;

        $programmes = Programme::where('code_pta', '=', $pta->code)->get();

        foreach ($programmes as $programme){
            $objectifs[$programme->code] = Objectif::where('code_prog', '=', $programme->code)->get();

            foreach ($objectifs[$programme->code] as $objectif){
                $actions[$objectif->code] = Action::where('code_obj', '=', $objectif->code)->get();

                foreach ($actions[$objectif->code] as $action){
                    $activites[$action->code] = Activite::where('code_action', '=', $action->code)->get();

                    foreach ($activites[$action->code] as $activite){
                        $taches[$activite->code] = Tache::where('code_act', '=', $activite->code)->get();
                    }
                }
            }
        }

        $data = compact('evaluation', 'programmes', 'objectifs', 'actions', 'activites', 'taches', 'annee', 'ministere');
        return view('evaluations.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectifs = $request['objectifs'];
        $actions = $request['actions'];
        $activites = $request['activites'];
        $taches = $request['taches'];

        foreach ($objectifs as $key => $value){
            $objectif = Objectif::where('id', '=', $key)->get()->first();

            $objectif->operations = $value['operation'];
            $objectif->niveau = $value['niveau'];
            $objectif->mnt_engage = $value['engage'];
            $objectif->mnt_ordonance = $value['ordonance'];
            $objectif->observations = $value['observation'];

            if($objectif->montant != 0){
                $objectif->taux_engage = ((doubleval($value['engage'])*100)/doubleval($objectif->montant));
                $objectif->taux_ordonance = ((doubleval($value['ordonance'])*100)/doubleval($objectif->montant));
            }

            $objectif->save();
        }
        //var_dump($objectifs);

        return redirect(route('evaluations.show', $id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
