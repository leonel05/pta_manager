<?php

namespace App\Http\Controllers;

use App\Models\Programme;
use App\Models\Pta;
use Illuminate\Http\Request;

class ProgrammeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $programmes = Programme::orderBy('code')->get();
        $pta_codes = Pta::orderBy('code')->get()->pluck('code', 'code');
        $pta_code = array();

        $data = compact('programmes', 'pta_codes', 'pta_code');
        return view('programmes.list', $data);
    }

    /**
     * Search and display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchList(Request $request)
    {
        $code = $request['code_pta'];
        $programmes = Programme::where('code_pta', '=', $code)->orderBy('code')->get();
        $pta_codes = Pta::orderBy('code')->get()->pluck('code', 'code');
        $pta = Pta::where('code', '=', $code)->first();
        $pta_code = array($pta->code);

        $data = compact('programmes', 'pta_codes', 'pta_code');
        return view('programmes.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $programme = new Programme();
        $pta_codes = Pta::orderBy('code')->get()->pluck('code', 'code');

        $data = compact('programme', 'pta_codes');

        return view('programmes.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nbr_prog = Programme::where('code_pta', '=', $request['code_pta'])->count();
        $request['code'] = $request['code_pta'].'-PROG'.($nbr_prog + 1);
        $programme = Programme::create($request->all());

        if ($programme){
            return redirect(route('programmes.create'));
        }

        $data = compact('programme');

        return view('programmes.form', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
