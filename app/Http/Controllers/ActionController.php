<?php

namespace App\Http\Controllers;

use App\Models\Action;
use App\Models\Objectif;
use Illuminate\Http\Request;

class ActionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $actions = Action::orderBy('code')->get();

        $data = compact('actions');
        return view('actions.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $action = new Action();
        $obj_codes = Objectif::orderBy('code')->get()->pluck('code', 'code');
        $mois = array(
            '1' => 'Janvier',
            '2' => 'Février',
            '3' => 'Mars',
            '4' => 'Avril',
            '5' => 'Mai',
            '6' => 'Juin',
            '7' => 'Juillet',
            '8' => 'Août',
            '9' => 'Septembre',
            '10' => 'Octobre',
            '11' => 'Novembre',
            '12' => 'Décembre',
        );

        $data = compact('action', 'mois',  'obj_codes');

        return view('actions.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $nbr_act = Action::where('code_obj', '=', $request['code_obj'])->count();
        $request['code'] = $request['code_obj'].'-A'.($nbr_act + 1);
        $request['abbrev'] = 'A'.($nbr_act + 1);

        $debut = $request['debut'];
        $fin = $request['fin'];

        if($fin < $debut){
            $request['nb_mois'] = (((int)$fin + 12) - (int)$debut) + 1;
        }
        else{
            $request['nb_mois'] = ((int)$fin - (int)$debut) + 1;
        }

        $action = Action::create($request->all());

        if ($action){
            return redirect(route('actions.create'));
        }

        $data = compact('action');

        return view('actions.form', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
