<?php

namespace App\Http\Controllers;

use App\Models\Evaluation;
use App\Models\Pta;
use App\Models\Rapport;
use Illuminate\Http\Request;

class IndexController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
      $this->middleware('auth');
  }

    public function index()
    {
        $nbr_pta  = Pta::all()->count();
        $nbr_eval  = Evaluation::all()->count();
        $nbr_rapport  = Rapport::all()->count();

        $pta = new Pta();
        $pta->setExternalConnection();

        $pta_external_list = $pta->get();
        $annees = $pta_external_list->pluck('anneeExerc', 'anneeExerc');
        $ministeres = $pta_external_list->pluck('codMinist', 'anneeExerc');

        foreach ($annees as $key => $annee){
            $list[$key.'-'.$ministeres[$key]] = 'PTA de l\'année '.$annee.' du '.$ministeres[$key];
        }

        $data = compact('nbr_pta', 'nbr_eval', 'nbr_rapport', 'list');
        return view('index', $data);
    }
}
