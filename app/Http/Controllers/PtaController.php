<?php

namespace App\Http\Controllers;

use App\Models\Action;
use App\Models\Activite;
use App\Models\Ministere;
use App\Models\Objectif;
use App\Models\Programme;
use App\Models\Pta;
use App\Models\Structure;
use App\Models\Tache;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PtaController extends Controller
{
    public function __construct(Request $request)
    {
        parent::__construct($request);
        //
    }

    public function index(Request $request){
        //
        //$pta = new Pta();
        //$pta->setExternalConnection();

        $pta_list = Pta::all();
        //$pta_external_list = $pta->get();
        //$annees = $pta_external_list->pluck('anneeExerc', 'anneeExerc');
        //$ministeres = $pta_external_list->pluck('codMinist', 'codMinist');

        $data = compact('pta_list');
        return view('pta.list', $data);
    }

    public function create(Request $request){
        //
        $pta = new Pta();
        $structures = Structure::orderBy('code')->get()->pluck('libelle', 'code');
        $ministeres = Ministere::orderBy('code')->get()->pluck('libelle', 'code');

        $message = $request->has('message') ? $request['message'] : null;
        $type = $request->has('type') ? $request['type'] : null;

        $data = compact('pta', 'ministeres', 'structures', 'message', 'type');

        return view('pta.form', $data);
    }

    public function store(Request $request){

        $request['code'] = 'PTA-'.$request['exercice'].'-'.$request['code_ministere'];

        if($request['code_structure']!=''){
            $structure = Structure::whereCode($request['code_structure'])->get()->first();
            $request['code_ministere'] = $structure->code_ministere;
        }
        $pta = Pta::create($request->all());

        if ($pta){
            $message = "Enregistrement du PTA effectué avec succès !";
            return redirect(route('pta.create', ['type' => 'success', 'message' => $message]));
        }
        $type = 'danger';
        $message = "Erreur lors de l'enregistrement du PTA";

        $data = compact('pta', 'type', 'message');

        return view('pta.form', $data);
    }

    public function show(Request $request, $id){
        $pta = Pta::where('id', '=',  $id)->get()->first();
        $annee = $pta->exercice;
        //$ministere = Ministere::where('code', '=', $pta->code_ministere)->get()->first();
        //$ministere = $ministere->libelle;
        $ministere = $pta->code_ministere;

        $programmes = Programme::where('code_pta', '=', $pta->code)->get();

        foreach ($programmes as $programme){
            $objectifs[$programme->code] = Objectif::where('code_prog', '=', $programme->code)->get();

            foreach ($objectifs[$programme->code] as $objectif){
                $actions[$objectif->code] = Action::where('code_obj', '=', $objectif->code)->get();

                foreach ($actions[$objectif->code] as $action){
                    $activites[$action->code] = Activite::where('code_action', '=', $action->code)->get();

                    foreach ($activites[$action->code] as $activite){
                        $taches[$activite->code] = Tache::where('code_act', '=', $activite->code)->get();
                    }
                }
            }
        }

        $data = compact('programmes', 'objectifs', 'actions', 'activites', 'taches', 'annee', 'ministere');
        return view('pta.show2', $data);
    }

    public function getImportForm(Request $request){
        $pta = new Pta();
        $pta->setExternalConnection();

        $pta_external_list = $pta->get();
        $annees = $pta_external_list->pluck('anneeExerc', 'anneeExerc');
        $ministeres = $pta_external_list->pluck('codMinist', 'anneeExerc');

        foreach ($annees as $key => $annee){
            $list[$key.'-'.$ministeres[$key]] = 'PTA de l\'année '.$annee.' du '.$ministeres[$key];
        }
        $data = compact('list');
        return view('pta.importsyasapta', $data);
    }

    public function import(Request $request){
        $details_pta = explode('-', $request['pta']);
        $annee = $details_pta[0];
        $ministere = $details_pta[1];


        $DB = DB::connection('mysql1');

        $pta = Pta::where('exercice', '=', $annee)
            ->where('code_ministere', '=', $ministere)
            ->get()
            ->first();
        if (!$pta){
            $originPta = $DB->table('pta')
                ->where('anneeExerc', '=', $annee)
                ->where('codMinist', '=', $ministere)
                ->get()
                ->first();

            $pta = new Pta();
            $pta->code = 'PTA-'.$annee.'-'.$ministere;
            $pta->obj_global = $originPta->objGlobal;
            $pta->code_ministere = $ministere;
            $pta->exercice = $annee;
            $pta->save();
            $newPta = true;
        }else{
            $newPta = false;
        }


        $programmes = $DB->table('programme')
            ->where('anneeExerc', '=', $annee)
            ->where('codMinist', '=', $ministere)
            ->get();

        foreach ($programmes as $programme){
            if($newPta){
                $nbr_prog = Programme::where('code_pta', $pta->code)->count();
                $prog = new Programme();
                $prog = $prog->create(array(
                   'code' => $pta->code.'-PROG'.($nbr_prog + 1),
                   'libelle' => $programme->libProgram,
                   'objectif' => $programme->objSpecifiqProgram,
                   'code_pta' => $pta->code
                ));
            }
            $objectifs[$programme->codProgram] = $DB->table('objectif')
                ->where('codProgram', $programme->codProgram)
                ->get();

            foreach ($objectifs[$programme->codProgram] as $objectif){
                $resultats[$objectif->codObjectif] = $DB->table('resultat')
                    ->where('codObjectif', $objectif->codObjectif)
                    ->get();

                foreach ($resultats[$objectif->codObjectif] as $resultat){
                    if($newPta){
                        $nbr_obj = Objectif::where('code_prog', $prog->code)->count();
                        $obj = new Objectif();
                        $obj = $obj->create(array(
                            'code' => $prog->code.'-O'.($nbr_obj + 1),
                            'abbrev' => 'O'.($nbr_obj + 1),
                            'libelle' => $objectif->libObjectif,
                            'resultat' => $resultat->libResultat,
                            'code_prog' => $prog->code
                        ));
                    }
                    $actions[$resultat->codResultat] = $DB->table('action')
                        ->where('codResultat', $resultat->codResultat)
                        ->get();

                    foreach ($actions[$resultat->codResultat] as $action){
                        if($newPta){
                            $nbr_actions = Action::where('code_obj', $obj->code)->count();
                            $a = new Action();
                            $a = $a->create(array(
                                'code' => $obj->code.'-A'.($nbr_actions + 1),
                                'abbrev' => 'A'.($nbr_actions + 1),
                                'libelle' => $action->libAction,
                                'montant' => $action->montant_engage,
                                'nb_mois' => $action->dateFinAction,
                                'code_obj' => $obj->code
                            ));
                        }
                        $activites[$action->codAction] = $DB->table('activite')
                            ->where('codAction', $action->codAction)
                            ->get();

                        foreach ($activites[$action->codAction] as $activite){
                            if($newPta){
                                $nbr_act = Activite::where('code_action', $a->code)->count();
                                $act = new Activite();
                                $act = $act->create(array(
                                    'code' => $a->code.'-ACT'.($nbr_act + 1),
                                    'abbrev' => 'ACT'.($nbr_act + 1),
                                    'libelle' => $activite->libActivite,
                                    'montant' => $activite->montantActivite,
                                    'nb_mois' => $activite->finPActivite,
                                    'poids' => $activite->poidsActivite,
                                    'code_action' => $a->code
                                ));
                            }
                            $taches[$activite->codActivite] = $DB->table('tache')
                                ->where('codActivite', $activite->codActivite)
                                ->get();

                            foreach ($taches[$activite->codActivite] as $tache) {
                                if ($newPta) {
                                    $nbr_taches = Tache::where('code_act', $act->code)->count();
                                    $tach = new Tache();
                                    $tach->create(array(
                                        'code' => $act->code . '-T' . ($nbr_taches + 1),
                                        'abbrev' => 'T' . ($nbr_taches + 1),
                                        'libelle' => $tache->libTache,
                                        'montant' => $tache->montantprogramme,
                                        'nb_mois' => $tache->finPTache,
                                        'poids' => $tache->poidsTache,
                                        'code_act' => $act->code
                                    ));
                                }
                            }
                        }
                    }
                }
            }
        }

        //dd($programmes);

        $data = compact('programmes', 'objectifs', 'resultats', 'actions', 'activites', 'taches', 'annee', 'ministere');
        return view('pta.show', $data);
    }

    public function getPtaInList(Request $request)
    {
        $keyword = strval($request['query']);
        $search_param = "{$keyword}%";

        var_dump($search_param); die();
        $results = DB::table('pta')->where('code', 'like', $search_param)->get();

        if (sizeof($results) > 0) {
            foreach($results as $result) {
                $ptaResult[] = $result->code;
            }
            return response()->json([
                'data' => json_encode($clientResult)
            ]);
        }
    }

}