<?php

namespace App\Http\Controllers;

use App\Models\Evaluation;
use App\Models\Ministere;
use App\Models\Pta;
use App\Models\Rapport;
use App\Models\Structure;
use Illuminate\Http\Request;

class RapportController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rapports = Rapport::orderBy('created_at', 'desc') ->get();

        foreach ($rapports as $rapport){
            $evaluations[$rapport->id] = Evaluation::whereId($rapport->evaluation)->get()->first();
        }

        $data = compact('evaluations', 'rapports');
        return view('rapports.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rapport = new Rapport();
        $evaluations = Evaluation::orderBy('created_at')->get()->pluck('titre', 'id');;

        $data = compact('rapport', 'evaluations');
        return view('rapports.form', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rapport = Rapport::create($request->all());

        if ($rapport){
            $message = "Enregistrement du rapport effectué avec succès !";
            return redirect(route('rapports.show', ['id' => $rapport->id, 'type' => 'success', 'message' => $message]));
        }
        $type = 'danger';
        $message = "Erreur lors de l'enregistrement du rapport";

        $data = compact('rapport', 'type', 'message');

        return view('rapports.form', $data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $rapport = Rapport::whereId($id)->get()->first();
        $evaluation = Evaluation::whereId($rapport->evaluation)->first();

        $details_pta = explode('-', $evaluation->code_pta);
        $ministere = Ministere::whereCode($details_pta[2])->first();

        $pta = Pta::whereCode($evaluation->code_pta)->first();
        if($pta->code_structure){
            $structure = Structure::whereCode($pta->code_structure)->first()->libelle;
        }else{
            $structure = null;
        }

        //var_dump($details_pta, $ministere); die();
        $data = compact('rapport', 'evaluation', 'ministere', 'structure', 'details_pta');

        return view('rapports.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $rapport = Rapport::whereId($id)->get()->first();
        $evaluation = Evaluation::whereId($rapport->evaluation)->get()->pluck('id');
        $evaluations = Evaluation::orderBy('created_at')->get()->pluck('titre', 'id');;


        $data = compact('rapport', 'evaluations', 'evaluation');
        return view('rapports.form', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rapport = Rapport::whereId($id)->get()->first();

        if ($rapport->update($request->all())){
            $message = "Modification du rapport effectuée avec succès !";
            return redirect(route('rapports.show', ['id' => $rapport->id, 'type' => 'success', 'message' => $message]));
        }
        $type = 'danger';
        $message = "Erreur lors de l'enregistrement des modifications ffectuée sur le rapport";

        $data = compact('rapport', 'type', 'message');

        return view('rapports.form', $data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
