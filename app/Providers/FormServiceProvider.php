<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Form;

class FormServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // Register custom form components
        Form::component('bsLabel', 'components.form.label', ['name', 'value']);
        Form::component('bsFile', 'components.form.file', ['name', 'model', 'avatar', 'alt', 'isView']);
        Form::component('bsInlineFile', 'components.form.inlinefile', ['name', 'model','isView']);
        Form::component('bsDate', 'components.form.date', ['name', 'readonly']);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
