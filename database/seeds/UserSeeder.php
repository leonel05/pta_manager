<?php

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\User;

class UserSeeder extends Seeder
{
  /**
  * Run the database seeds.
  *
  * @return void
  */
  public function run()
  {
    $adminRole = Role::whereName('administrateur')->first();
    $userRole = Role::whereName('utilisateur')->first();

    $user = User::create(array(
      'name'      => 'toto',
      'email'         => 'j.doe@codingo.me',
      'password'      => bcrypt('password'),
      'remember_token' => str_random(10)

    ));
    $user->assignRole($adminRole);

    $user = User::create(array(
      'name'      => 'tata',
      'email'         => 'jane.doe@codingo.me',
      'password'      => bcrypt('janesPassword'),
      'remember_token' => str_random(10)
    ));
    $user->assignRole($userRole);
  }
}
