<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateObjectifsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('objectif', function (Blueprint $table){
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('code_prog', 50);
            $table->string('abbrev', 10);
            $table->text('libelle');
            $table->text('resultat');
            $table->unsignedInteger('montant')->nullable()->default(0);
            $table->unsignedInteger('nb_mois')->nullable();
            $table->string('responsable', 10)->nullable();
            $table->string('associees', 255)->nullable();
            $table->double('poids', 6,2)->nullable();

            $table->text('operations')->nullable();
            $table->unsignedInteger('niveau')->nullable()->default(0);
            $table->unsignedInteger('mnt_engage')->nullable()->default(0);
            $table->double('taux_engage',5,3)->nullable()->default(0);
            $table->unsignedInteger('mnt_ordonance')->nullable()->default(0);
            $table->double('taux_ordonance', 5,3)->nullable()->default(0);
            $table->text('observations')->nullable();

            //$table->double('poids_physique', 6,2)->nullable();
            //$table->double('poids_financier', 6,2)->nullable();
            //$table->double('poids_temporel', 6,2)->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('objectif');
    }
}
