<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateActivitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activite', function (Blueprint $table){
            $table->increments('id');
            $table->string('code')->unique();
            $table->string('code_action', 50);
            $table->string('abbrev', 10);
            $table->text('libelle');
            $table->unsignedInteger('montant')->nullable()->default(0);
            $table->unsignedInteger('nb_mois')->nullable();
            $table->string('associees', 255)->nullable();
            $table->double('poids', 6,2)->nullable();

            $table->text('operations')->nullable();
            $table->unsignedInteger('niveau')->nullable()->default(0);
            $table->unsignedInteger('mnt_engage')->nullable()->default(0);
            $table->double('taux_engage',5,3)->nullable()->default(0);
            $table->unsignedInteger('mnt_ordonance')->nullable()->default(0);
            $table->double('taux_ordonance', 5,3)->nullable()->default(0);
            $table->text('observations')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activite');
    }
}
