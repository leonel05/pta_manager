<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePtasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pta', function (Blueprint $table){
            $table->increments('id');
            $table->string('code')->unique();
            $table->text('obj_global')->nullable();
            $table->string('code_ministere', 10);
            $table->string('code_structure', 10)->nullable();
            $table->string('exercice', 4);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pta');
    }
}
