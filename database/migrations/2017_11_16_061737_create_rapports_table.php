<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRapportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rapport', function (Blueprint $table){
            $table->increments('id');
            $table->longText('resume');
            $table->longText('introduction');
            $table->longText('objectifs');
            $table->longText('resultats');
            $table->longText('resources');
            $table->longText('activites_prevues');
            $table->longText('activites_non_prevues');
            $table->longText('bilan_financier');
            $table->longText('difficultes_recommandations');
            $table->longText('conclusion');
            $table->unsignedInteger('evaluation');
            $table->unsignedInteger('auteur')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rapport');
    }
}
