<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStructuresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('structure', function (Blueprint $table){
           $table->increments('id');
           $table->string('code', 10);
           $table->string('libelle', 255);
           $table->string('code_typestructure', 10);
           $table->string('code_ministere',10);

           $table->timestamps();

            /*$table->foreign('code_typestructure')->references('code')->on('typestructure')->onDelete('restrict')->onUpdate('cascade');
            $table->foreign('code_ministere')->references('code')->on('ministere')->onDelete('restrict')->onUpdate('cascade');*/
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('structure');
    }
}
